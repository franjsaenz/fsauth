package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.exception.BadRequestException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class ValidationUtilsTest {

    @Test
    public void isNotNull() {
        log.info("Test object is not null");

        Platform p = new Platform();

        Exception resp = null;

        try {
            ValidationUtils.notNull(p, "object");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNull();
    }

    @Test
    public void isNull() {
        log.info("Test object is null");

        Platform p = null;

        Exception resp = null;

        try {
            ValidationUtils.notNull(p, "object");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
    }

    @Test
    public void isNotEmpty() {
        log.info("Test string is not empty");

        String s = "some";

        Exception resp = null;

        try {
            ValidationUtils.notEmpty(s, "string");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNull();
    }

    @Test
    public void isEmpty() {
        log.info("Test string is empty");

        String s = "";

        Exception resp = null;

        try {
            ValidationUtils.notEmpty(s, "string");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
    }

    @Test
    public void isNotDeleted() {
        log.info("Test entity is not deleted");

        Platform p = new Platform();

        Exception resp = null;

        try {
            ValidationUtils.notDeleted(p, "entity");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNull();
    }

    @Test
    public void isDeleted() {
        log.info("Test entity is deleted");

        Platform p = new Platform();
        p.setDeleted(Instant.now());

        Exception resp = null;

        try {
            ValidationUtils.notDeleted(p, "entity");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
    }

    @Test
    public void expirationAfterNotBefore() {
        log.info("Test expiration after notBefore");

        Instant expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
        Instant notBefore = Instant.now();

        Exception resp = null;

        try {
            ValidationUtils.notBeforeNotAfterExpiration(expiration, notBefore);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNull();
    }

    @Test
    public void expirationBeforeNotBefore() {
        log.info("Test expiration before notBefore");

        Instant expiration = Instant.now();
        Instant notBefore = Instant.now().plus(30, ChronoUnit.MINUTES);

        Exception resp = null;

        try {
            ValidationUtils.notBeforeNotAfterExpiration(expiration, notBefore);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(BadRequestException.class);
    }

}
