package com.fsquiroz.fsauth.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MCreatePlatform {

    private String name;
    private String description;
    private String platformIdentifier;
    private String password;
}
