package com.fsquiroz.fsauth.runner;

import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Slf4j
@Component
@Profile("!test")
@RequiredArgsConstructor
public class SuperUserCreationRunner implements ApplicationRunner {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    @Override
    public void run(ApplicationArguments args) {
        checkSuper();
    }

    private void checkSuper() {
        long count = userRepository.count();
        if (count > 0) {
            return;
        }
        log.info("Creating first super user 'super@fsauth.com' with password '123456'. PLEASE CHANGE ASAP!!!");
        User u = new User();
        u.setPlatformIdentifier("super@fsauth.com");
        u.setCreated(Instant.now());
        u.setRole(Role.SUPER);
        u.setPassword(passwordEncoder.encode("123456"));
        u.setPasswordType(User.PasswordType.V_1);
        userRepository.save(u);
    }

}
