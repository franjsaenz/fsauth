package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.Instant;
import java.util.Collection;
import java.util.Collections;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Credential extends Filterable implements UserDetails {

    @DBRef
    private User user;

    @DBRef
    private User issuer;

    private Instant issuedAt;

    private Instant expiration;

    private Instant notBefore;

    @Transient
    private String token;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        if (user != null && user.getRole() != null) {
            return Collections.singleton(new SimpleGrantedAuthority("ROLE_" + user.getRole().getName()));
        }
        return Collections.emptyList();
    }

    @Override
    public String getUsername() {
        return user != null ? user.getPlatformIdentifier() : null;
    }

    @Override
    public String getPassword() {
        return user != null ? user.getPassword() : null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return this.user != null && (this.expiration == null || !this.expiration.isAfter(Instant.now()));
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.user != null && (this.notBefore == null || !this.notBefore.isBefore(Instant.now()));
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return this.user != null && (this.expiration == null || !this.expiration.isAfter(Instant.now()));
    }

    @Override
    public boolean isEnabled() {
        return this.deleted == null && this.user != null && this.user.getDeleted() != null;
    }

}
