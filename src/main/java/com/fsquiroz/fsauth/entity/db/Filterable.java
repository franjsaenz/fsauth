package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;

@Getter
@Setter
@ToString(callSuper = true)
public abstract class Filterable extends Entity {

    @DBRef
    protected Platform platform;

}
