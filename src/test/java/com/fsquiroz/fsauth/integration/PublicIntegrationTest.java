package com.fsquiroz.fsauth.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MLogin;
import com.fsquiroz.fsauth.mock.CredentialMock;
import com.fsquiroz.fsauth.mock.PlatformMock;
import com.fsquiroz.fsauth.mock.TokenMock;
import com.fsquiroz.fsauth.mock.UserMock;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class PublicIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PlatformMock platformMock;

    @Autowired
    private UserMock userMock;

    @Autowired
    private CredentialMock credentialMock;

    @Autowired
    private TokenMock tokenMock;

    private Platform p1;

    private Platform p2;

    private User root;

    private User admin1;

    private User admin2;

    private User client1;

    private User client2;

    private User user1;

    private Credential forRoot;

    private Credential forAdmin1;

    private Credential forAdmin2;

    private Credential forClient1;

    private Credential forClient2;

    private Credential forUser1;

    private Token rootToken;

    private Token admin1Token;

    private Token admin2Token;

    private Token client1Token;

    private Token client2Token;

    private Token user1Token;

    @BeforeEach
    public void setup() {
        p1 = platformMock.create(null, null);
        p2 = platformMock.create(null, null);
        root = userMock.create("root", null, Role.SUPER, null);
        admin1 = userMock.create("admin1", null, Role.PLATFORM_ADMIN, p1);
        admin2 = userMock.create("admin2", null, Role.PLATFORM_ADMIN, p2);
        client1 = userMock.create("client1", null, Role.PLATFORM_CLIENT, p1);
        client2 = userMock.create("client2", null, Role.PLATFORM_CLIENT, p2);
        user1 = userMock.create("user1", null, Role.PLATFORM_USER, p1);
        forRoot = credentialMock.create(root, null, null, null);
        forAdmin1 = credentialMock.create(admin1, root, null, null);
        forAdmin2 = credentialMock.create(admin2, root, null, null);
        forClient1 = credentialMock.create(client1, root, null, null);
        forClient2 = credentialMock.create(client2, root, null, null);
        forUser1 = credentialMock.create(user1, client1, null, null);
        rootToken = tokenMock.create(root, Token.Type.LOGIN);
        admin1Token = tokenMock.create(admin1, Token.Type.LOGIN);
        admin2Token = tokenMock.create(admin2, Token.Type.LOGIN);
        client1Token = tokenMock.create(client1, Token.Type.LOGIN);
        client2Token = tokenMock.create(client2, Token.Type.LOGIN);
        user1Token = tokenMock.create(user1, Token.Type.LOGIN);
    }

    @AfterEach
    public void cleanup() {
        tokenMock.cleanup();
        credentialMock.cleanup();
        userMock.cleanup();
        platformMock.cleanup();
    }

    @Test
    public void batchLoginAsPasswordTests() throws Exception {
        List<LoginTestCase> cases = new ArrayList<>();
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        null,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        batchLoginTest(cases);
    }

    @Test
    public void batchLoginAsPasswordlessTests() throws Exception {
        List<LoginTestCase> cases = new ArrayList<>();
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        null,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forRoot,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forAdmin1,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isOk())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forAdmin2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forClient1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forClient2,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        root,
                        rootToken,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin1,
                        admin1Token,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        admin2,
                        admin2Token,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client1,
                        client1Token,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        client2,
                        client2Token,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        cases.add(
                new LoginTestCase(
                        user1,
                        user1Token,
                        forUser1,
                        Collections.singletonList(MockMvcResultMatchers.status().isUnauthorized())
                )
        );
        batchLoginTest(cases);
    }

    private void batchLoginTest(List<LoginTestCase> cases) throws Exception {
        List<TestError> errors = new ArrayList<>();
        for (LoginTestCase tc : cases) {
            String title = String.format(
                    "Test login as %s%s with issuer %s",
                    tc.as != null ? tc.as.getPlatformIdentifier() : "anonymous",
                    tc.token == null ? " with method 'PASSWORD'" : " with method 'PASSWORDLESS'",
                    tc.issuer != null ? tc.issuer.getUsername() : "anonymous"
            );
            log.info(title);
            User as = tc.as;
            Token token = tc.token;
            MLogin login = new MLogin(as.getPlatformIdentifier(), as.getPassword(), token != null ? token.getId() : null);
            ResultActions result = mockMvc.perform(login(tc.issuer, login, token == null ? LoginMethod.PASSWORD : LoginMethod.PASSWORDLESS));
            for (ResultMatcher matcher : tc.matchers) {
                try {
                    result.andExpect(matcher);
                } catch (AssertionError ae) {
                    TestError error = new TestError();
                    error.title = title;
                    error.error = ae;
                    errors.add(error);
                }
            }
        }
        AssertionError ae = null;
        for (TestError error : errors) {
            log.error("Test fail:", error.error);
            log.error("Error in test: {}", error.title);
            if (ae == null) {
                ae = new AssertionError(error.title);
            }
        }
        if (ae != null) {
            throw ae;
        }
    }

    @Test
    public void getLoggedInAsRoot() throws Exception {
        log.info("Test get logged in as root");
        mockMvc.perform(login(forRoot))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLoggedInAsAdmin1() throws Exception {
        log.info("Test get logged in as admin1");
        mockMvc.perform(login(forAdmin1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLoggedInAsAdmin2() throws Exception {
        log.info("Test get logged in as admin2");
        mockMvc.perform(login(forAdmin2))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLoggedInAsClient1() throws Exception {
        log.info("Test get logged in as client1");
        mockMvc.perform(login(forClient1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLoggedInAsClient2() throws Exception {
        log.info("Test get logged in as client2");
        mockMvc.perform(login(forClient2))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getLoggedInAsUser1() throws Exception {
        log.info("Test get logged in as user1");
        mockMvc.perform(login(forUser1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsRoot() throws Exception {
        log.info("Test status as root");
        mockMvc.perform(index(forRoot))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsAdmin1() throws Exception {
        log.info("Test status as admin1");
        mockMvc.perform(index(forAdmin1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsAdmin2() throws Exception {
        log.info("Test status as admin2");
        mockMvc.perform(index(forAdmin2))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsClient1() throws Exception {
        log.info("Test status as client1");
        mockMvc.perform(index(forClient1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsClient2() throws Exception {
        log.info("Test status as client2");
        mockMvc.perform(index(forClient2))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsUser1() throws Exception {
        log.info("Test status as user1");
        mockMvc.perform(index(forUser1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void statusAsAnonymous() throws Exception {
        log.info("Test status as anonymous");
        mockMvc.perform(index(null))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void logoutAsRoot() throws Exception {
        log.info("Test logout as root");
        mockMvc.perform(logout(forRoot))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void logoutAsAdmin() throws Exception {
        log.info("Test logout as admin");
        mockMvc.perform(logout(forAdmin1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void logoutAsUser() throws Exception {
        log.info("Test logout as root");
        mockMvc.perform(logout(forUser1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void logoutAsClient() throws Exception {
        log.info("Test logout as root");
        mockMvc.perform(logout(forClient1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void logoutAsAnonymous() throws Exception {
        log.info("Test logout as anonymous");
        mockMvc.perform(logout(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder login(Credential who, MLogin login, LoginMethod method) throws Exception {
        if (method == null) {
            method = LoginMethod.PASSWORD;
        }
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/login")
                .param("method", method.getName())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(login));
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder login(Credential who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/login")
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder logout(Credential who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/login")
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder index(Credential who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/")
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    @Data
    @AllArgsConstructor
    private static class LoginTestCase {
        User as;
        Token token;
        Credential issuer;
        List<ResultMatcher> matchers;
    }

    private static class TestError {
        String title;
        AssertionError error;
    }

}
