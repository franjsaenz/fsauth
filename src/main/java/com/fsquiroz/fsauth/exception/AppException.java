package com.fsquiroz.fsauth.exception;

import lombok.Getter;

import java.util.Map;

@Getter
public class AppException extends RuntimeException {

    private final ErrorCode code;

    private final Map<String, Object> meta;

    protected AppException(ErrorCode code, Map<String, Object> meta, String message) {
        super(message);
        this.code = code;
        this.meta = meta;
    }

    protected AppException(ErrorCode code, Map<String, Object> meta, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
        this.meta = meta;
    }

}
