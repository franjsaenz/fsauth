package com.fsquiroz.fsauth.repository;

import com.fsquiroz.fsauth.entity.db.*;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;

@Repository
@RequiredArgsConstructor
public class SearchRepository {

    private final MongoOperations dbOperations;

    public Page<User> searchUsers(String term, Platform platform, Collection<Role> roles, boolean withDeleted, Pageable pageable) {
        Query q = new Query();
        if (!withDeleted) {
            q.addCriteria(Criteria.where("deleted").is(null));
        }
        if (platform != null) {
            q.addCriteria(Criteria.where("platform").is(platform));
        }
        if (roles != null && !roles.isEmpty()) {
            q.addCriteria(Criteria.where("role").in(roles));
        }
        if (term != null && !term.isEmpty()) {
            q.addCriteria(search(term, "platformIdentifier"));
        }
        return buildPage(q, User.class, pageable);
    }

    public Page<Platform> searchPlatforms(String term, boolean withDeleted, Pageable pageable) {
        Query q = new Query();
        if (!withDeleted) {
            q.addCriteria(Criteria.where("deleted").is(null));
        }
        if (term != null && !term.isEmpty()) {
            q.addCriteria(
                    new Criteria().orOperator(
                            search(term, "name"),
                            search(term, "description")
                    )
            );
        }
        return buildPage(q, Platform.class, pageable);
    }

    public Page<Credential> searchCredentials(User user, Platform platform, Boolean valid, Pageable pageable) {
        Query q = new Query();
        q.addCriteria(Criteria.where("deleted").is(null));
        if (user != null) {
            q.addCriteria(Criteria.where("user").is(user));
        }
        if (platform != null) {
            q.addCriteria(Criteria.where("platform").is(platform));
        }
        if (valid != null) {
            if (valid) {
                q.addCriteria(new Criteria().andOperator(
                        new Criteria().orOperator(
                                Criteria.where("expiration").is(null),
                                Criteria.where("expiration").gte(Instant.now())
                        ),
                        new Criteria().orOperator(
                                Criteria.where("notBefore").is(null),
                                Criteria.where("notBefore").lte(Instant.now())
                        )
                ));
            } else {
                q.addCriteria(new Criteria().orOperator(
                        Criteria.where("expiration").lt(Instant.now()),
                        Criteria.where("notBefore").gt(Instant.now())
                ));
            }
        }
        return buildPage(q, Credential.class, pageable);
    }

    private Criteria search(String term, String field) {
        return Criteria.where(field).regex(".*" + term + ".*", "i");
    }

    private <T extends Entity> Page<T> buildPage(Query q, Class<T> clazz, Pageable page) {
        long count = dbOperations.count(q, clazz);
        q.with(page);
        List<T> result = dbOperations.find(q, clazz);
        return new PageImpl<>(result, page, count);
    }

}
