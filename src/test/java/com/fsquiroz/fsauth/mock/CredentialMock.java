package com.fsquiroz.fsauth.mock;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.repository.CredentialRepository;
import com.fsquiroz.fsauth.security.SecurityCheck;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.util.Assert;

import java.time.Instant;

@Component
@ActiveProfiles("test")
public class CredentialMock extends Mock<Credential> {

    private SecurityCheck securityCheck;

    public CredentialMock(CredentialRepository credentialRepository, SecurityCheck securityCheck) {
        super(credentialRepository);
        this.securityCheck = securityCheck;
    }

    public Credential create(User authorized, User issuer, Instant expiration, Instant notBefore) {
        Assert.notNull(authorized, "'authorized' must not be null");
        Credential c = new Credential();
        c.setPlatform(authorized.getPlatform());
        c.setUser(authorized);
        c.setIssuer(issuer);
        c.setExpiration(expiration);
        c.setNotBefore(notBefore);
        return save(c);
    }

    public void authenticate(MockHttpServletRequestBuilder request, Credential c) {
        if (c != null) {
            securityCheck.generateToken(c);
            request.header("Authorization", "Bearer " + c.getToken());
            if (c.getPlatform() != null) {
                request.header("Platform-Id", c.getPlatform().getId());
            }
        }
    }

}
