package com.fsquiroz.fsauth.mock;

import com.fsquiroz.fsauth.entity.db.Token;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.repository.TokenRepository;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Component
@ActiveProfiles("test")
public class TokenMock extends Mock<Token> {

    public TokenMock(TokenRepository tokenRepository) {
        super(tokenRepository);
    }

    public Token create(User user, Token.Type type) {
        Token t = new Token();
        t.setPlatform(user.getPlatform());
        t.setUser(user);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));
        t.setType(type);
        return save(t);
    }

}
