package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MLogin;
import com.fsquiroz.fsauth.entity.json.MResetPassword;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.exception.AppException;
import com.fsquiroz.fsauth.exception.BadRequestException;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.exception.UnauthorizedException;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.repository.TokenRepository;
import com.fsquiroz.fsauth.repository.UserRepository;
import com.fsquiroz.fsauth.service.declaration.ICredentialService;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.UUID;
import java.util.function.Supplier;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserService implements IUserService {

    private final PasswordEncoder encoder;

    private final ICredentialService credentialService;

    private final UserRepository userRepository;

    private final TokenRepository tokenRepository;

    private final SearchRepository searchRepository;

    @Override
    public Credential login(MLogin login, LoginMethod method, User issuer, Instant expiration, Instant notBefore) {
        ValidationUtils.notNull(login, "login");
        ValidationUtils.notEmpty(login.getPlatformIdentifier(), "platformIdentifier");
        if (issuer != null && (Role.PLATFORM_USER.equals(issuer.getRole()) || null == issuer.getRole())) {
            throw UnauthorizedException.byIssuerWithoutPermission(issuer);
        }
        if (LoginMethod.PASSWORD.equals(method)) {
            ValidationUtils.notEmpty(login.getPassword(), "password");
        } else if (LoginMethod.PASSWORDLESS.equals(method)) {
            if (issuer == null || Role.PLATFORM_USER.equals(issuer.getRole())) {
                throw UnauthorizedException.byPasswordlessWithoudRole(issuer);
            }
        } else {
            throw BadRequestException.byInvalidLoginMethod(method);
        }
        User user = userRepository.findByPlatformAndPlatformIdentifier(issuer != null ? issuer.getPlatform() : null, login.getPlatformIdentifier())
                .orElseThrow(() -> {
                    if (log.isDebugEnabled()) {
                        log.debug("No user found for:\nlogin: {}\nIssuer: {}", login, issuer);
                    }
                    return UnauthorizedException.byInvalidCredentials();
                });
        if (user.getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(user);
        }
        if (LoginMethod.PASSWORD.equals(method)) {
            if (!encoder.matches(login.getPassword(), user.getPassword())) {
                throw UnauthorizedException.byInvalidCredentials();
            }
            return credentialService.create(user, issuer, expiration, notBefore);
        } else {
            if (Role.SUPER.equals(user.getRole())) {
                throw UnauthorizedException.byInvalidCredentials();
            }
            if (login.getToken() != null && !login.getToken().isEmpty()) {
                validate(user, login.getToken(), Token.Type.LOGIN, issuer, UnauthorizedException::byInvalidCredentials);
                return credentialService.create(user, issuer, expiration, notBefore);
            } else {
                Token t = generate(Token.Type.LOGIN, user, issuer);
                return generate(t, user, issuer);
            }
        }
    }

    @Override
    public Page<User> search(String term, Platform platform, List<Role> roles, boolean withDeleted, Pageable pageable) {
        return searchRepository.searchUsers(term, platform, roles, withDeleted, pageable);
    }

    @Override
    public User get(String id) {
        return userRepository.findById(id).orElseThrow(() -> NotFoundException.byId(User.class, id));
    }

    @Override
    public User get(String platformIdentifier, Platform platform) {
        return userRepository.findByPlatformAndPlatformIdentifier(platform, platformIdentifier).orElseThrow(() -> NotFoundException.byPlatformIdentifier(platformIdentifier));
    }

    @Override
    public User create(MUser model, User creator, Platform platform) {
        ValidationUtils.notNull(model, "user");
        ValidationUtils.notEmpty(model.getPlatformIdentifier(), "platformIdentifier");
        ValidationUtils.notNull(model.getRole(), "role");
        if (creator != null && !Role.SUPER.equals(creator.getRole())) {
            if (Role.SUPER.equals(model.getRole())) {
                throw BadRequestException.byNonSuperCreatingSuper(creator);
            }
        }
        User u = userRepository.findByPlatformAndPlatformIdentifier(platform, model.getPlatformIdentifier()).orElse(null);
        if (u != null) {
            throw BadRequestException.byExistingPlatformIdentifier(model.getPlatformIdentifier());
        }
        u = new User();
        u.setPlatform(platform);
        u.setCreator(creator);
        u.setCreated(Instant.now());
        u.setRole(model.getRole());
        u.setPlatformIdentifier(model.getPlatformIdentifier());
        u.setAttributes(model.getAttributes());
        if (Role.PLATFORM_CLIENT.equals(model.getRole())) {
            u.setPasswordType(User.PasswordType.NOT_SET);
        } else if (model.getPassword() != null && !model.getPassword().isEmpty()) {
            u.setPassword(encoder.encode(model.getPassword()));
            u.setPasswordType(User.PasswordType.V_1);
        } else {
            u.setPasswordType(User.PasswordType.PASSWORDLESS);
        }
        return userRepository.save(u);
    }

    @Override
    public User update(User original, MUser edit, User updater) {
        ValidationUtils.notNull(edit, "user");
        ValidationUtils.notDeleted(original, "entity");
        original.setAttributes(edit.getAttributes());
        original.setUpdated(Instant.now());
        original.setUpdater(updater);
        return userRepository.save(original);
    }

    @Override
    public User delete(User original, User deleter) {
        ValidationUtils.notDeleted(original, "entity");
        original.setDeleted(Instant.now());
        original.setDeleter(deleter);
        return userRepository.save(original);
    }

    @Override
    public User changePassword(User original, MResetPassword resetPassword) {
        ValidationUtils.notDeleted(original, "entity");
        ValidationUtils.notNull(resetPassword, "resetPassword");
        ValidationUtils.notEmpty(resetPassword.getOldPassword(), "oldPassword");
        ValidationUtils.notEmpty(resetPassword.getNewPassword(), "newPassword");
        if (original.getPassword() != null && !encoder.matches(resetPassword.getOldPassword(), original.getPassword())) {
            throw UnauthorizedException.byInvalidOldPassword();
        }
        original.setPassword(encoder.encode(resetPassword.getNewPassword()));
        return userRepository.save(original);
    }

    @Override
    public Credential resetPassword(User original, User issuer) {
        Token t = generate(Token.Type.RESET_PASSWORD, original, issuer);
        return generate(t, original, issuer);
    }

    @Override
    public User resetPassword(User original, MResetPassword resetPassword, User issuer) {
        ValidationUtils.notDeleted(original, "entity");
        ValidationUtils.notEmpty(resetPassword.getToken(), "token");
        ValidationUtils.notEmpty(resetPassword.getNewPassword(), "newPassword");
        validate(original, resetPassword.getToken(), Token.Type.RESET_PASSWORD, issuer, UnauthorizedException::byInvalidToken);
        original.setPassword(encoder.encode(resetPassword.getNewPassword()));
        return userRepository.save(original);
    }

    private Token generate(Token.Type type, User user, User issuer) {
        Token t = new Token();
        t.setId(UUID.randomUUID().toString());
        t.setCreated(Instant.now());
        t.setCreator(issuer);
        t.setPlatform(user.getPlatform());
        t.setType(type);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));
        t.setUser(user);
        return tokenRepository.save(t);
    }

    private Credential generate(Token t, User user, User issuer) {
        Credential c = new Credential();
        c.setId("not-valid");
        c.setIssuer(issuer);
        c.setIssuedAt(t.getCreated());
        c.setExpiration(t.getExpiration());
        c.setUser(user);
        c.setPlatform(user.getPlatform());
        c.setToken(t.getId());
        return c;
    }

    private void validate(User user, String token, Token.Type expected, User issuer, Supplier<AppException> toThrow) {
        Token t = tokenRepository.findById(token)
                .orElseThrow(() -> {
                    if (log.isDebugEnabled()) {
                        log.debug("No token found: {}", token);
                    }
                    return toThrow.get();
                });
        if (!user.equals(t.getUser())) {
            if (log.isDebugEnabled()) {
                log.debug("Mismatch token for user.\ntoken: {}\nuser: {}", t, user);
            }
            deleteToken(t, issuer);
            throw toThrow.get();
        } else if (t.getDeleted() != null) {
            if (log.isDebugEnabled()) {
                log.debug("Token already used.\ntoken: {}", t);
            }
            throw toThrow.get();
        } else if (!expected.equals(t.getType())) {
            if (log.isDebugEnabled()) {
                log.debug("Mismatch token type.\nexpected type: {}\ntoken: {}", expected, t);
            }
            deleteToken(t, issuer);
            throw toThrow.get();
        } else if (t.getExpiration().isBefore(Instant.now())) {
            if (log.isDebugEnabled()) {
                log.debug("Token expired.\nToken: {}", t);
            }
            deleteToken(t, issuer);
            throw toThrow.get();
        }
        deleteToken(t, issuer);
    }

    private void deleteToken(Token t, User issuer) {
        t.setDeleted(Instant.now());
        t.setDeleter(issuer);
        tokenRepository.save(t);
    }
}
