package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.json.MCreatePlatform;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.exception.BadRequestException;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.repository.PlatformRepository;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class PlatformServiceTest {

    @MockBean
    private IUserService userService;

    @MockBean
    private PlatformRepository platformRepository;

    @MockBean
    private SearchRepository searchRepository;

    private PlatformService service;

    @BeforeEach
    public void setup() {
        service = new PlatformService(userService, platformRepository, searchRepository);
    }

    @Test
    public void search() {
        log.info("Test search");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        p.setDescription("Description");
        List<Platform> list = Collections.singletonList(p);
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Platform> page = new PageImpl<>(list, pageRequest, 1);

        Mockito.when(searchRepository.searchPlatforms(any(), anyBoolean(), any()))
                .thenReturn(page);

        Page<Platform> resp = service.search(null, false, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .containsExactly(p);
    }

    @Test
    public void get() {
        log.info("Test get platform");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        p.setDescription("Description");

        Mockito.when(platformRepository.findById(any()))
                .thenReturn(Optional.of(p));

        Platform resp = service.get("1");

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("1");
        assertThat(resp.getName())
                .isEqualTo("Name");
        assertThat(resp.getDescription())
                .isEqualTo("Description");
    }

    @Test
    public void getNotFound() {
        log.info("Test get platform not found");

        Mockito.when(platformRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;

        try {
            service.get("1");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
    }

    @Test
    public void create() {
        log.info("Test create platform");

        MCreatePlatform mp = new MCreatePlatform();
        mp.setName("Name");
        mp.setDescription("Description");
        mp.setPlatformIdentifier("some");
        mp.setPassword("password");

        Mockito.when(platformRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Platform resp = service.create(mp, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getName())
                .isEqualTo("Name");
        assertThat(resp.getDescription())
                .isEqualTo("Description");
    }

    @Test
    public void createErrorInUser() {
        log.info("Test create platform with error creating user");

        log.info("Test create platform");

        MCreatePlatform mp = new MCreatePlatform();
        mp.setName("Name");
        mp.setDescription("Description");
        mp.setPlatformIdentifier("some");
        mp.setPassword("password");

        Mockito.when(platformRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(userService.create(any(), any(), any()))
                .thenThrow(BadRequestException.byMissingParam("platformIdentifier"));

        Exception resp = null;

        try {
            service.create(mp, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void update() {
        log.info("Test update platform");

        Instant created = Instant.now().minus(3, ChronoUnit.DAYS);
        Platform p = new Platform();
        p.setId("1");
        p.setCreated(created);
        p.setName("Name");
        p.setDescription("Description");

        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");

        Mockito.when(platformRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Platform resp = service.update(p, mp, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getName())
                .isEqualTo("New name");
        assertThat(resp.getDescription())
                .isEqualTo("New description");
    }

    @Test
    public void delete() {
        log.info("Test delete platform");

        Instant created = Instant.now().minus(3, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(1, ChronoUnit.DAYS);
        Platform p = new Platform();
        p.setId("1");
        p.setCreated(created);
        p.setUpdated(updated);
        p.setName("Name");
        p.setDescription("Description");

        Mockito.when(platformRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Platform resp = service.delete(p, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getName())
                .isEqualTo("Name");
        assertThat(resp.getDescription())
                .isEqualTo("Description");
    }

}
