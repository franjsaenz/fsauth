package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCredential;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.entity.json.MUser;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.*;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class BuildServiceTest {

    private BuildService service;

    @BeforeEach
    public void setup() {
        service = new BuildService();
    }

    @Test
    public void buildUsers() {
        log.info("Test build users");

        Pageable pageRequest = PageRequest.of(0, 10);
        Instant created = Instant.now().minus(30, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(20, ChronoUnit.DAYS);
        Instant deleted = Instant.now().minus(10, ChronoUnit.DAYS);
        User u = new User();
        u.setId("1");
        u.setCreated(created);
        u.setUpdated(updated);
        u.setDeleted(deleted);
        u.setPlatformIdentifier("some-identifier");
        u.setPassword("some-password");
        u.setRole(Role.SUPER);

        List<User> list = Collections.singletonList(u);
        Page<User> page = new PageImpl<>(list, pageRequest, 1);

        Page<MUser> resp = service.users(page);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getTotalElements())
                .isEqualTo(1);
        assertThat(resp.getPageable())
                .isNotNull();
        assertThat(resp.getPageable().getPageSize())
                .isEqualTo(10);
        assertThat(resp.getPageable().getPageNumber())
                .isEqualTo(0);
        assertThat(resp.getContent())
                .isNotNull();
        assertThat(resp.getContent())
                .hasSize(1);
        assertThat(resp.getContent().get(0))
                .isNotNull();
        MUser el = resp.getContent().get(0);
        assertThat(el.getId())
                .isEqualTo("1");
        assertThat(el.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(el.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(el.getDeleted())
                .isCloseTo(deleted, within(1, ChronoUnit.MILLIS));
        assertThat(el.getPlatformIdentifier())
                .isEqualTo("some-identifier");
        assertThat(el.getPassword())
                .isNull();
        assertThat(el.getRole())
                .isEqualTo(Role.SUPER);
    }

    @Test
    public void buildCredential() {
        log.info("Test build credential");

        User u = new User();
        u.setId("1");
        u.setRole(Role.PLATFORM_ADMIN);
        User iss = new User();
        iss.setId("2");
        iss.setRole(Role.SUPER);

        Pageable pageRequest = PageRequest.of(0, 10);
        Instant created = Instant.now().minus(30, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(20, ChronoUnit.DAYS);
        Instant deleted = Instant.now().minus(10, ChronoUnit.DAYS);
        Instant expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
        Instant notBefore = Instant.now();
        Credential c = new Credential();
        c.setId("1");
        c.setCreated(created);
        c.setUpdated(updated);
        c.setDeleted(deleted);
        c.setIssuedAt(created);
        c.setIssuer(iss);
        c.setUser(u);
        c.setExpiration(expiration);
        c.setNotBefore(notBefore);
        c.setToken("token");

        List<Credential> list = Collections.singletonList(c);
        Page<Credential> page = new PageImpl<>(list, pageRequest, 1);

        Page<MCredential> resp = service.credentials(page);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getTotalElements())
                .isEqualTo(1);
        assertThat(resp.getPageable())
                .isNotNull();
        assertThat(resp.getPageable().getPageSize())
                .isEqualTo(10);
        assertThat(resp.getPageable().getPageNumber())
                .isEqualTo(0);
        assertThat(resp.getContent())
                .isNotNull();
        assertThat(resp.getContent())
                .hasSize(1);
        assertThat(resp.getContent().get(0))
                .isNotNull();
        MCredential el = resp.getContent().get(0);
        assertThat(el.getId())
                .isEqualTo("1");
        assertThat(el.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(el.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(el.getDeleted())
                .isCloseTo(deleted, within(1, ChronoUnit.MILLIS));
        assertThat(el.getIssuedAt())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(el.getIssuer())
                .isNotNull();
        assertThat(el.getUser())
                .isNotNull();
        assertThat(el.getToken())
                .isEqualTo("token");
        assertThat(el.getExpiration())
                .isCloseTo(expiration, within(1, ChronoUnit.MILLIS));
        assertThat(el.getNotBefore())
                .isCloseTo(notBefore, within(1, ChronoUnit.MILLIS));
    }

    @Test
    public void buildPlatform() {
        log.info("Test build platform");

        Pageable pageRequest = PageRequest.of(0, 10);
        Instant created = Instant.now().minus(30, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(20, ChronoUnit.DAYS);
        Instant deleted = Instant.now().minus(10, ChronoUnit.DAYS);
        Platform p = new Platform();
        p.setId("1");
        p.setCreated(created);
        p.setUpdated(updated);
        p.setDeleted(deleted);
        p.setName("Name");
        p.setDescription("Description");

        List<Platform> list = Collections.singletonList(p);
        Page<Platform> page = new PageImpl<>(list, pageRequest, 1);

        Page<MPlatform> resp = service.platforms(page);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getTotalElements())
                .isEqualTo(1);
        assertThat(resp.getPageable())
                .isNotNull();
        assertThat(resp.getPageable().getPageSize())
                .isEqualTo(10);
        assertThat(resp.getPageable().getPageNumber())
                .isEqualTo(0);
        assertThat(resp.getContent())
                .isNotNull();
        assertThat(resp.getContent())
                .hasSize(1);
        assertThat(resp.getContent().get(0))
                .isNotNull();
        MPlatform el = resp.getContent().get(0);
        assertThat(el.getId())
                .isEqualTo("1");
        assertThat(el.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(el.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(el.getDeleted())
                .isCloseTo(deleted, within(1, ChronoUnit.MILLIS));
        assertThat(el.getName())
                .isEqualTo("Name");
        assertThat(el.getDescription())
                .isEqualTo("Description");
    }

}
