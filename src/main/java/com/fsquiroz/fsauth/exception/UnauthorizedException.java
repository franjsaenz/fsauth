package com.fsquiroz.fsauth.exception;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.User;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends AppException {

    private UnauthorizedException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static UnauthorizedException byInvalidCredentials() {
        return new UnauthorizedException(ErrorCode.U_INVALID_CREDENTIALS, null, "Invalid credentials");
    }

    public static UnauthorizedException byDeletedUser(User who) {
        Assert.notNull(who, "'who' can not be null");
        Map<String, Object> user = ofUser(who);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("user", user);
        return new UnauthorizedException(ErrorCode.U_SUSPENDED_USER, meta, "Can not get a suspended user");
    }

    public static UnauthorizedException byInvalidOldPassword() {
        return new UnauthorizedException(ErrorCode.U_INVALID_OLD_PASSWORD, null, "Invalid old password");
    }

    public static UnauthorizedException byInvalidToken() {
        return new UnauthorizedException(ErrorCode.U_INVALID_TOKEN, null, "Invalid token");
    }

    public static UnauthorizedException byInsufficientPermission() {
        return new UnauthorizedException(ErrorCode.U_INSUFFICIENT_PERMISSIONS, null, "Insufficient permission");
    }

    public static UnauthorizedException byPasswordlessWithoudRole(User who) {
        Map<String, Object> issuer = ofUser(who);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("issuer", issuer);
        return new UnauthorizedException(ErrorCode.U_PASSWORDLESS_WITHOUT_ROLE, meta, "Passwordless authentication can not be perform without an issuer or issuer with role 'PLATFORM_USER'");
    }

    public static UnauthorizedException byDeletedCredential(Credential credential) {
        Map<String, Object> c = ofCredential(credential);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("credential", c);
        return new UnauthorizedException(ErrorCode.U_DELETED_CREDENTIAL, meta, "Credential disabled");
    }

    public static UnauthorizedException byIssuerWithoutPermission(User issuer) {
        Map<String, Object> iss = ofUser(issuer);
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("issuer", iss);
        return new UnauthorizedException(ErrorCode.U_ISSUER_WITHOUT_PERMISSION, meta, "Users without role or with role 'PLATFORM_USER' can not perform login");
    }

    public static UnauthorizedException byTokenExpired() {
        return new UnauthorizedException(ErrorCode.U_TOKEN_EXPIRED, null, "Token expired");
    }

    private static Map<String, Object> ofUser(User user) {
        Map<String, Object> u = null;
        if (user != null) {
            u = new LinkedHashMap<>();
            u.put("id", user.getId());
            u.put("platformIdentifier", user.getPlatformIdentifier());
            u.put("role", user.getRole());
            if (user.getPlatform() != null) {
                u.put("platformId", user.getPlatform().getId());
            }
            if (user.getDeleted() != null) {
                u.put("deleted", user.getDeleted());
            }
        }
        return u;
    }

    private static Map<String, Object> ofCredential(Credential credential) {
        Map<String, Object> c = null;
        if (credential != null) {
            c = new LinkedHashMap<>();
            c.put("id", credential.getId());
            c.put("user", ofUser(credential.getUser()));
            c.put("issueAt", credential.getIssuedAt());
            if (credential.getPlatform() != null) {
                c.put("platformId", credential.getPlatform().getId());
            }
            if (credential.getDeleted() != null) {
                c.put("deleted", credential.getDeleted());
            }
        }
        return c;
    }

}
