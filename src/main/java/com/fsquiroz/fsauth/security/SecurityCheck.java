package com.fsquiroz.fsauth.security;

import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.exception.UnauthorizedException;
import com.fsquiroz.fsauth.repository.CredentialRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.crypto.spec.SecretKeySpec;
import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@Component
@Slf4j
public class SecurityCheck {

    private final Key jwtSecret;

    private final CredentialRepository credentialRepository;

    public SecurityCheck(
            @Value("${security.jwt.secret}") String jwtSecret,
            @Value("${security.jwt.algorithm:HmacSHA512}") String algorithm,
            CredentialRepository credentialRepository
    ) {
        this.jwtSecret = getKey(jwtSecret, algorithm);
        this.credentialRepository = credentialRepository;
    }

    private Key getKey(String jwtSecret, String algorithm) {
        var bytes = Base64.decodeBase64(jwtSecret);
        return new SecretKeySpec(bytes, algorithm);
    }

    Credential get(String id) {
        Credential c = credentialRepository.findById(id)
                .orElseThrow(() -> NotFoundException.byId(Credential.class, id));
        if (c.getUser() == null) {
            throw UnauthorizedException.byInvalidCredentials();
        }
        if (c.getUser().getDeleted() != null) {
            throw UnauthorizedException.byDeletedUser(c.getUser());
        }
        if (c.getDeleted() != null) {
            throw UnauthorizedException.byDeletedCredential(c);
        }
        return c;
    }

    public Credential authenticate(String token) {
        Claims claims = Jwts.parserBuilder()
                .setSigningKey(jwtSecret)
                .build()
                .parseClaimsJws(token)
                .getBody();
        String id = claims.get("credentialId", String.class);
        return get(id);
    }

    public void generateToken(Credential c) {
        Claims claims = Jwts.claims().setSubject(c.getUser().getPlatformIdentifier());
        claims.put("credentialId", c.getId());
        claims.put("userId", c.getUser().getId());
        if (c.getCreated() != null) {
            claims.setIssuedAt(Date.from(c.getIssuedAt()));
        } else {
            Instant issuedAt = Instant.now();
            claims.setIssuedAt(Date.from(issuedAt));
        }
        if (c.getUser().getPlatform() != null) {
            claims.put("platformId", c.getUser().getPlatform().getId());
            claims.put("platformName", c.getUser().getPlatform().getName());
        }
        if (c.getIssuer() != null) {
            claims.setIssuer(c.getIssuer().getPlatformIdentifier());
        }
        if (c.getExpiration() != null) {
            claims.setExpiration(Date.from(c.getExpiration()));
        }
        if (c.getNotBefore() != null) {
            claims.setNotBefore(Date.from(c.getNotBefore()));
        }
        String token = Jwts.builder()
                .setClaims(claims)
                .signWith(jwtSecret)
                .compact();
        c.setToken(token);
    }

    public Optional<Credential> getAuthentication() {
        Credential c = null;
        SecurityContext context = SecurityContextHolder.getContext();
        if (context != null) {
            Authentication a = context.getAuthentication();
            if (a != null) {
                Object o = a.getPrincipal();
                if (o instanceof Credential) {
                    c = (Credential) o;
                }
            }
        }
        return Optional.ofNullable(c);
    }

    public User getAuthenticated() {
        return getAuthentication()
                .map(Credential::getUser)
                .orElseThrow(UnauthorizedException::byInsufficientPermission);
    }

    public boolean canGetUser(User user) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to access entity User with id '{}'",
                    id,
                    user != null ? user.getId() : "null"
            );
        }
        try {
            return hasAccess(user, id);
        } catch (Access a) {
            return a.isGranted();
        }
    }

    public boolean canUseCredential(Credential credential) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to access entity Credential with id '{}'",
                    id,
                    credential != null ? credential.getId() : "null"
            );
        }
        try {
            return hasAccess(credential, id);
        } catch (Access a) {
            return a.isGranted();
        }
    }

    public boolean canUpdateUser(User user) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to update entity User with id '{}'",
                    id,
                    user != null ? user.getId() : "null"
            );
        }
        try {
            return hasAccess(user, id);
        } catch (Access a) {
            return a.isGranted();
        }
    }

    public boolean canDeleteCredential(Credential credential) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to delete entity Credential with id '{}'",
                    id,
                    credential != null ? credential.getId() : "null"
            );
        }
        try {
            Credential c = getRedacted(id);
            User u = getUser(c, id);
            objectOrPlatformNulls(credential, id);
            isAdminFromPlatform(credential, u, id);
            return false;
        } catch (Access a) {
            return a.isGranted();
        }
    }

    public boolean canGetPlatform(Platform platform) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to get entity Platform with id '{}'",
                    id,
                    platform != null ? platform.getId() : "null"
            );
        }
        try {
            Credential c = getRedacted(id);
            User u = getUser(c, id);
            objectOrPlatformNulls(platform, id);
            boolean isSamePlatform = platform.equals(u.getPlatform());
            if (!isSamePlatform && log.isDebugEnabled()) {
                log.debug("[{}] Access denied by not being from same platform", id);
            } else if (isSamePlatform && log.isTraceEnabled()) {
                log.trace("[{}] Access granted by being from same platform", id);
            }
            return isSamePlatform;
        } catch (Access a) {
            return a.isGranted();
        }
    }

    private boolean hasAccess(User user, String id) throws Access {
        Credential c = getRedacted(id);
        User u = getUser(c, id);
        objectOrPlatformNulls(user, id);
        isAdminFromPlatform(user, u, id);
        boolean isLoggedIn = user.equals(u);
        if (isLoggedIn && log.isTraceEnabled()) {
            log.trace("[{}] Access granted by being same as logged in", id);
        } else if (!isLoggedIn && log.isDebugEnabled()) {
            log.debug("[{}] Access denied by mismatch logged in", id);
        }
        return isLoggedIn;
    }

    private boolean hasAccess(Credential credential, String id) throws Access {
        Credential c = getRedacted(id);
        User u = getUser(c, id);
        objectOrPlatformNulls(credential, id);
        isAdminFromPlatform(credential, u, id);
        boolean isOwner = u.equals(credential.getUser());
        if (!isOwner && log.isDebugEnabled()) {
            log.debug("[{}] Access denied by not being owner", id);
        } else if (isOwner && log.isTraceEnabled()) {
            log.trace("[{}] Access granted by being owner", id);
        }
        return isOwner;
    }

    public boolean canResetPassword(User user) {
        String id = requestId();
        if (log.isDebugEnabled()) {
            log.debug(
                    "[{}] Attempting to update entity User with id '{}'",
                    id,
                    user != null ? user.getId() : "null"
            );
        }
        try {
            Credential c = getRedacted(id);
            User u = getUser(c, id);
            objectOrPlatformNulls(user, id);
            boolean isSamePlatform = user.getPlatform().equals(u.getPlatform()) && (u.getRole() == Role.PLATFORM_ADMIN || u.getRole() == Role.PLATFORM_CLIENT);
            if (isSamePlatform) {
                if (log.isTraceEnabled()) {
                    log.trace("[{}] Access granted by being platform's admin or client", id);
                }
                return true;
            } else if (log.isDebugEnabled()) {
                log.debug("[{}] Access denied by not being platform's admin or client", id);
            }
            return false;
        } catch (Access a) {
            return a.isGranted();
        }
    }

    private String requestId() {
        return UUID.randomUUID().toString().substring(32, 36);
    }

    private Credential getRedacted(String id) {
        Credential c = getAuthentication().orElse(null);
        if (log.isTraceEnabled()) {
            log.trace("[{}] Access requested by: {}", id, c);
        }
        return c;
    }

    private User getUser(Credential c, String id) throws Access {
        User u = c != null ? c.getUser() : null;
        if (u == null) {
            if (log.isDebugEnabled()) {
                log.debug("[{}] Access denied by unauthorized", id);
            }
            throw new AccessDenied();
        }
        if (u.getRole() == Role.SUPER) {
            if (log.isTraceEnabled()) {
                log.trace("[{}] Access granted by being super user", id);
            }
            throw new AccessGranted();
        }
        return u;
    }

    private void objectOrPlatformNulls(Entity entity, String id) throws Access {
        if (entity == null) {
            if (log.isTraceEnabled()) {
                log.trace("[{}] Access granted by entity null", id);
            }
            throw new AccessGranted();
        }
        if (entity instanceof Filterable) {
            Filterable filterable = (Filterable) entity;
            if (filterable.getPlatform() == null) {
                if (log.isDebugEnabled()) {
                    log.debug("[{}] Access denied by entity's platform null", id);
                }
                throw new AccessDenied();
            }
        }
    }

    private void isAdminFromPlatform(Filterable filterable, User u, String id) throws Access {
        boolean isAdminFromPlatform = filterable.getPlatform().equals(u.getPlatform()) && u.getRole() == Role.PLATFORM_ADMIN;
        if (isAdminFromPlatform) {
            if (log.isTraceEnabled()) {
                log.trace("[{}] Access granted by being platform's admin", id);
            }
            throw new AccessGranted();
        }
    }

    @Getter
    private abstract class Access extends Exception {
        private final boolean granted;

        private Access(boolean granted) {
            this.granted = granted;
        }
    }

    private class AccessGranted extends Access {
        private AccessGranted() {
            super(true);
        }
    }

    private class AccessDenied extends Access {
        private AccessDenied() {
            super(false);
        }
    }

}
