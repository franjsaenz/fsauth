package com.fsquiroz.fsauth.service.declaration;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCredential;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.entity.json.MUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

import java.util.ArrayList;
import java.util.List;

public interface IBuildService {

    MUser user(User user);

    MCredential credential(Credential credential);

    MPlatform platform(Platform platform);

    default List<MUser> users(List<User> users) {
        List<MUser> mu = new ArrayList<>();
        for (User u : users) {
            mu.add(user(u));
        }
        return mu;
    }

    default List<MCredential> credentials(List<Credential> credentials) {
        List<MCredential> mc = new ArrayList<>();
        for (Credential c : credentials) {
            mc.add(credential(c));
        }
        return mc;
    }

    default List<MPlatform> platforms(List<Platform> platforms) {
        List<MPlatform> mp = new ArrayList<>();
        for (Platform p : platforms) {
            mp.add(platform(p));
        }
        return mp;
    }

    default Page<MUser> users(Page<User> users) {
        return new PageImpl<>(users(users.getContent()), users.getPageable(), users.getTotalElements());
    }

    default Page<MCredential> credentials(Page<Credential> credentials) {
        return new PageImpl<>(credentials(credentials.getContent()), credentials.getPageable(), credentials.getTotalElements());
    }

    default Page<MPlatform> platforms(Page<Platform> platforms) {
        return new PageImpl<>(platforms(platforms.getContent()), platforms.getPageable(), platforms.getTotalElements());
    }

}
