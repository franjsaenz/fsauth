package com.fsquiroz.fsauth.mock;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.repository.PlatformRepository;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

@Component
@ActiveProfiles("test")
public class PlatformMock extends Mock<Platform> {

    public PlatformMock(PlatformRepository platformRepository) {
        super(platformRepository);
    }

    public Platform create(String name, String description) {
        if (name == null) {
            name = mockString();
        }
        if (description == null) {
            description = mockString();
        }
        Platform p = new Platform();
        p.setName(name);
        p.setDescription(description);
        return save(p);
    }

}
