package com.fsquiroz.fsauth.exception;

import com.fsquiroz.fsauth.entity.db.User;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundException extends AppException {

    private NotFoundException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static NotFoundException byId(Class<?> clazz, String id) {
        Map<String, Object> meta = ofClass(clazz);
        meta.put("id", id);
        return new NotFoundException(ErrorCode.NF_BY_ID, meta, "Entity not found");
    }

    public static NotFoundException byPlatformIdentifier(String platformIdentifier) {
        Map<String, Object> meta = ofClass(User.class);
        meta.put("platformIdentifier", platformIdentifier);
        return new NotFoundException(ErrorCode.NF_BY_PLATFORM_IDENTIFIER, meta, "Entity not found");
    }

    private static Map<String, Object> ofClass(Class<?> clazz) {
        Assert.notNull(clazz, "'clazz' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", clazz.getSimpleName());
        return meta;
    }

}
