package com.fsquiroz.fsauth.controller;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCreatePlatform;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.entity.json.MResponse;
import com.fsquiroz.fsauth.security.SecurityCheck;
import com.fsquiroz.fsauth.service.declaration.IBuildService;
import com.fsquiroz.fsauth.service.declaration.IPlatformService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/platforms")
@RequiredArgsConstructor
public class PlatformController {

    private final IPlatformService platformService;

    private final IBuildService buildService;

    private final SecurityCheck securityCheck;

    @GetMapping
    public ResponseEntity<?> search(
            @RequestParam(required = false) String term,
            @RequestParam(required = false, defaultValue = "false") boolean withDeleted,
            Pageable pageable
    ) {
        Page<Platform> platforms = platformService.search(term, withDeleted, pageable);
        return ResponseEntity.ok(buildService.platforms(platforms));
    }

    @PostMapping
    public ResponseEntity<?> create(
            @RequestBody MCreatePlatform platform
    ) {
        User creator = securityCheck.getAuthenticated();
        Platform p = platformService.create(platform, creator);
        return new ResponseEntity(buildService.platform(p), HttpStatus.CREATED);
    }

    @GetMapping("/{platformId}")
    public ResponseEntity<?> get(@PathVariable String platformId) {
        Platform p = platformService.get(platformId);
        return ResponseEntity.ok(buildService.platform(p));
    }

    @PutMapping("/{platformId}")
    public ResponseEntity<?> update(
            @PathVariable String platformId,
            @RequestBody MPlatform platform
    ) {
        User updater = securityCheck.getAuthenticated();
        Platform p = platformService.get(platformId);
        p = platformService.update(p, platform, updater);
        return ResponseEntity.ok(buildService.platform(p));
    }

    @DeleteMapping("/{platformId}")
    public ResponseEntity<?> delete(@PathVariable String platformId) {
        User deleter = securityCheck.getAuthenticated();
        Platform p = platformService.get(platformId);
        platformService.delete(p, deleter);
        return ResponseEntity.ok(MResponse.of("Platform deleted"));
    }
}
