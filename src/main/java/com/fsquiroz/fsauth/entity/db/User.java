package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Map;

@Document
@Getter
@Setter
@ToString(callSuper = true, exclude = {"password"})
public class User extends Filterable {

    private Role role;

    private String platformIdentifier;

    private String password;

    private PasswordType passwordType;

    private Map<String, Object> attributes;

    public enum PasswordType {
        NOT_SET, PASSWORDLESS, V_1
    }
}
