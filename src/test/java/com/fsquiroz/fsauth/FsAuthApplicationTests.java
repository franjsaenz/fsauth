package com.fsquiroz.fsauth;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class FsAuthApplicationTests {

    @Test
    public void contextLoads() {
        log.info("Test context loads");
    }

}
