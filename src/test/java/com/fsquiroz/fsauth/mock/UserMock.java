package com.fsquiroz.fsauth.mock;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.test.context.ActiveProfiles;

@Component
@ActiveProfiles("test")
public class UserMock extends Mock<User> {

    private PasswordEncoder encoder;

    public UserMock(UserRepository userRepository, PasswordEncoder encoder) {
        super(userRepository);
        this.encoder = encoder;
    }

    public User create(String platformIdentifier, String password, Role role, Platform platform) {
        if (platformIdentifier == null) {
            platformIdentifier = mockString();
        }
        if (password == null) {
            password = mockString();
        }
        User u = new User();
        u.setPlatform(platform);
        u.setPlatformIdentifier(platformIdentifier);
        u.setPassword(encoder.encode(password));
        u.setRole(role);
        return save(u);
    }

}
