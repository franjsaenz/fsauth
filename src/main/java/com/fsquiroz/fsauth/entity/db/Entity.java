package com.fsquiroz.fsauth.entity.db;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.time.Instant;

@Data
@EqualsAndHashCode(of = "id")
@ToString(exclude = {"creator", "updater", "deleter"})
public abstract class Entity {

    @Id
    protected String id;

    protected Instant created;

    @DBRef
    protected User creator;

    protected Instant updated;

    @DBRef
    protected User updater;

    protected Instant deleted;

    @DBRef
    private User deleter;

}
