package com.fsquiroz.fsauth.service;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class TimeUtilsTest {

    @Test
    public void formatToStandardInstant2Str() {
        log.info("Format instant to standard format Instant to String");

        Instant when = Instant.EPOCH;

        String resp = TimeUtils.standard(when);

        assertThat(resp)
                .isEqualTo("1970-01-01T00:00:00.000+0000");
    }

    @Test
    public void formatToStandardStr2Instant() {
        log.info("Format instant to standard format String to Instant");

        String when = "1970-01-01T00:00:00.000+0000";

        Instant resp = TimeUtils.standard(when);

        assertThat(resp)
                .isCloseTo(Instant.EPOCH, within(1, ChronoUnit.MILLIS));
    }

}
