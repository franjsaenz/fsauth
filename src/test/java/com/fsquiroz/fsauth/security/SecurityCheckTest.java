package com.fsquiroz.fsauth.security;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.exception.UnauthorizedException;
import com.fsquiroz.fsauth.repository.CredentialRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

import static java.util.Optional.of;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class SecurityCheckTest {

    private final String jwtSecret = "VqBGFiMvkp7eXHWFpNY+tfGb2r1mFSWP9vk0ASQsy9GEPaTm/lF5pEy6jyC+" +
            "ZOhqlhr5XRQpfqSgwUlJUCoGbO8dkzWeddnkDQ6SeKCW7YnLdT37odnjuvldRmeW5V8i//" +
            "qxoGogZe73ywof0QXLm5gMjRQd0s0gks3LgmulCPw=";

    private final String algorithm = "HmacSHA512";

    @Mock
    private CredentialRepository credentialRepository;

    private SecurityCheck service;

    @BeforeEach
    void setUp() {
        service = new SecurityCheck(jwtSecret, algorithm, credentialRepository);
    }

    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void getCredential() {
        log.info("Test get credential by id");

        Credential c = new Credential();
        c.setId("1");
        c.setUser(new User());

        when(credentialRepository.findById(any())).thenReturn(of(c));

        Credential resp = service.get("1");

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("1");
    }

    @Test
    public void getCredentialNotfound() {
        log.info("Test get non existing credential by id");

        assertThatThrownBy(() -> service.get("1"))
                .isExactlyInstanceOf(NotFoundException.class);
    }

    @Test
    public void getCredentialWithoutUser() {
        log.info("Test get credential by id without user");

        Credential c = new Credential();
        c.setId("1");

        when(credentialRepository.findById(any())).thenReturn(of(c));

        assertThatThrownBy(() -> service.get("1"))
                .isExactlyInstanceOf(UnauthorizedException.class)
                .hasMessage("Invalid credentials");
    }

    @Test
    public void getCredentialWithUserDeleted() {
        log.info("Test get credential by id with user deleted");

        User u = new User();
        u.setId("2");
        u.setDeleted(Instant.now());
        Credential c = new Credential();
        c.setId("1");
        c.setUser(u);

        when(credentialRepository.findById(any())).thenReturn(of(c));

        assertThatThrownBy(() -> service.get("1"))
                .isExactlyInstanceOf(UnauthorizedException.class)
                .hasMessage("Can not get a suspended user");
    }

    @Test
    public void getDeletedCredential() {
        log.info("Test get deleted credential");

        User u = new User();
        u.setId("2");
        Credential c = new Credential();
        c.setId("1");
        c.setDeleted(Instant.now());
        c.setUser(u);

        when(credentialRepository.findById(any())).thenReturn(of(c));

        assertThatThrownBy(() -> service.get("1"))
                .isExactlyInstanceOf(UnauthorizedException.class)
                .hasMessage("Credential disabled");
    }

    @Test
    public void authenticate() {
        log.info("Test authenticate by jwt");

        User u = new User();
        u.setId("2");
        u.setPlatformIdentifier("demo@example.com");
        Credential c = new Credential();
        c.setId("1234567890");
        c.setUser(u);

        when(credentialRepository.findById(any())).thenReturn(of(c));

        service.generateToken(c);

        String token = c.getToken();

        Credential resp = service.authenticate(token);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("1234567890");
    }

    @Test
    public void generateToken() {
        log.info("Test generate token");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User user = new User();
        user.setId("u2");
        user.setCreated(Instant.now());
        user.setPlatformIdentifier("some-identifier");
        user.setPlatform(platform);

        User issuer = new User();
        issuer.setId("u1");
        issuer.setCreated(Instant.now());
        issuer.setPlatformIdentifier("issuer-identifier");
        issuer.setPlatform(platform);

        Instant expiration = Instant.now().plus(100, ChronoUnit.DAYS);
        Instant notBefore = Instant.now().minus(100, ChronoUnit.DAYS);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreator(issuer);
        credential.setCreated(Instant.now());
        credential.setUser(user);
        credential.setIssuer(issuer);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);
        credential.setExpiration(expiration);
        credential.setNotBefore(notBefore);

        service.generateToken(credential);

        assertThat(credential)
                .isNotNull();
        assertThat(credential.getToken())
                .isNotBlank();
    }

    @Test
    public void generateTokenWithoutCreatedDate() {
        log.info("Test generate token without created date");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User user = new User();
        user.setId("u2");
        user.setCreated(Instant.now());
        user.setPlatformIdentifier("some-identifier");
        user.setPlatform(platform);

        User issuer = new User();
        issuer.setId("u1");
        issuer.setCreated(Instant.now());
        issuer.setPlatformIdentifier("issuer-identifier");
        issuer.setPlatform(platform);

        Instant expiration = Instant.now().plus(100, ChronoUnit.DAYS);
        Instant notBefore = Instant.now().minus(100, ChronoUnit.DAYS);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreator(issuer);
        credential.setUser(user);
        credential.setIssuer(issuer);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);
        credential.setExpiration(expiration);
        credential.setNotBefore(notBefore);

        service.generateToken(credential);

        assertThat(credential)
                .isNotNull();
        assertThat(credential.getToken())
                .isNotBlank();
    }

    @Test
    public void getAuthentication() {
        log.info("Test get authenticated credential");

        User u = new User();
        u.setId("2");
        Credential c = new Credential();
        c.setId("1");
        c.setUser(u);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(c, null, c.getAuthorities()));

        Optional<Credential> resp = service.getAuthentication();

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isNotEmpty();
        assertThat(resp)
                .contains(c);
    }

    @Test
    public void getAuthenticated() {
        log.info("Test get authenticated user");

        User u = new User();
        u.setId("2");
        Credential c = new Credential();
        c.setId("1");
        c.setUser(u);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(c, null, c.getAuthorities()));

        User resp = service.getAuthenticated();

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("2");
    }

    @Test
    public void getUnauthenticated() {
        log.info("Test get unauthenticated user");
        assertThatThrownBy(() -> service.getAuthenticated())
                .isExactlyInstanceOf(UnauthorizedException.class)
                .hasMessage("Insufficient permission");
    }

    @Test
    public void getAuthenticatedWithoutUser() {
        log.info("Test get authenticated without user");

        Credential c = new Credential();
        c.setId("1");

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(c, null, c.getAuthorities()));

        assertThatThrownBy(() -> service.getAuthenticated())
                .isExactlyInstanceOf(UnauthorizedException.class)
                .hasMessage("Insufficient permission");
    }

    @Test
    public void canGetUserAsRoot() {
        log.info("Test canGetUser as root");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("super");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetUserAsPlatformAdmin() {
        log.info("Test canGetUser as platform administrator");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("super");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetUserAsSelf() {
        log.info("Test canGetUser as self");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(toAccess);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetUserAsOther() {
        log.info("Test canGetUser as other");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canGetUserNotAuthenticated() {
        log.info("Test canGetUser as other");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canGetNullUser() {
        log.info("Test canGetUser for no user");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("super");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(null);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetUserWithoutPlatform() {
        log.info("Test canGetUser without platform");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("super");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetUser(toAccess);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canUseCredentialAsRoot() {
        log.info("Test canGetCredential as root");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("super");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toGet = new Credential();
        toGet.setId("c2");
        toGet.setUser(toAccess);
        toGet.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUseCredential(toGet);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUseCredentialAsPlatformAdmin() {
        log.info("Test canGetCredential as platform administrator");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("admin");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toGet = new Credential();
        toGet.setId("c2");
        toGet.setUser(toAccess);
        toGet.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUseCredential(toGet);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUseCredentialAsSelf() {
        log.info("Test canGetCredential as self");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(toAccess);
        credential.setIssuedAt(credential.getCreated());

        Credential toGet = new Credential();
        toGet.setId("c2");
        toGet.setUser(toAccess);
        toGet.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUseCredential(toGet);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUseCredentialAsOther() {
        log.info("Test canGetCredential as other");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toGet = new Credential();
        toGet.setId("c2");
        toGet.setUser(toAccess);
        toGet.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUseCredential(toGet);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canUpdateUserAsRoot() {
        log.info("Test canUpdateUser as root");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("super");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUpdateUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUpdateUserAsPlatformAdmin() {
        log.info("Test canUpdateUser as platform administrator");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("super");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUpdateUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUpdateUserAsSelf() {
        log.info("Test canUpdateUser as self");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(toAccess);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUpdateUser(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canUpdateUserAsOther() {
        log.info("Test canUpdateUser as other");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());
        credential.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canUpdateUser(toAccess);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canDeleteCredentialAsRoot() {
        log.info("Test canDeleteCredential as root");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("super");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toDelete = new Credential();
        toDelete.setId("c2");
        toDelete.setUser(toAccess);
        toDelete.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canDeleteCredential(toDelete);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canDeleteCredentialAsPlatformAdmin() {
        log.info("Test canDeleteCredential as platform administrator");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_ADMIN);
        authenticated.setPlatformIdentifier("admin");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toDelete = new Credential();
        toDelete.setId("c2");
        toDelete.setUser(toAccess);
        toDelete.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canDeleteCredential(toDelete);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canDeleteCredentialAsSelf() {
        log.info("Test canDeleteCredential as self");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(toAccess);
        credential.setIssuedAt(credential.getCreated());

        Credential toDelete = new Credential();
        toDelete.setId("c2");
        toDelete.setUser(toAccess);
        toDelete.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canDeleteCredential(toDelete);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canDeleteCredentialAsOther() {
        log.info("Test canDeleteCredential as other");
        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        Credential toDelete = new Credential();
        toDelete.setId("c2");
        toDelete.setUser(toAccess);
        toDelete.setPlatform(platform);

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canDeleteCredential(toDelete);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canGetPlatformAsSuper() {
        log.info("Test canGetPlatform from same platform");

        Platform toGet = new Platform();
        toGet.setId("p1");
        toGet.setCreated(Instant.now());

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("root");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetPlatform(toGet);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetPlatformFromSamePlatform() {
        log.info("Test canGetPlatform from same platform");

        Platform toGet = new Platform();
        toGet.setId("p1");
        toGet.setCreated(Instant.now());

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(toGet);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetPlatform(toGet);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canGetPlatformFromOtherPlatform() {
        log.info("Test canGetPlatform from other platform");

        Platform toGet = new Platform();
        toGet.setId("p1");
        toGet.setCreated(Instant.now());

        Platform other = new Platform();
        other.setId("p2");
        other.setCreated(Instant.now());

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_USER);
        authenticated.setPlatformIdentifier("other");
        authenticated.setPlatform(other);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canGetPlatform(toGet);

        assertThat(resp)
                .isFalse();
    }

    @Test
    public void canResetPasswordAsRoot() {
        log.info("Test canResetPassword as root");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.SUPER);
        authenticated.setPlatformIdentifier("super");

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canResetPassword(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canResetPasswordFromSamePlatform() {
        log.info("Test canResetPassword from same platform");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_CLIENT);
        authenticated.setPlatformIdentifier("client");
        authenticated.setPlatform(platform);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canResetPassword(toAccess);

        assertThat(resp)
                .isTrue();
    }

    @Test
    public void canResetPasswordFromOtherPlatform() {
        log.info("Test canResetPassword from other platform");

        Platform platform = new Platform();
        platform.setId("p1");
        platform.setCreated(Instant.now());
        Platform other = new Platform();
        other.setId("p2");
        other.setCreated(Instant.now());

        User toAccess = new User();
        toAccess.setId("u2");
        toAccess.setCreated(Instant.now());
        toAccess.setRole(Role.PLATFORM_USER);
        toAccess.setPlatformIdentifier("some-identifier");
        toAccess.setPlatform(platform);

        User authenticated = new User();
        authenticated.setId("u1");
        authenticated.setCreated(Instant.now());
        authenticated.setRole(Role.PLATFORM_CLIENT);
        authenticated.setPlatformIdentifier("client");
        authenticated.setPlatform(other);

        Credential credential = new Credential();
        credential.setId("c1");
        credential.setCreated(Instant.now());
        credential.setUser(authenticated);
        credential.setIssuedAt(credential.getCreated());

        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(credential, null, credential.getAuthorities()));

        boolean resp = service.canResetPassword(toAccess);

        assertThat(resp)
                .isFalse();
    }

}
