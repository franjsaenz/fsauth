package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.repository.CredentialRepository;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.security.SecurityCheck;
import com.fsquiroz.fsauth.service.declaration.ICredentialService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

@Service
@RequiredArgsConstructor
public class CredentialService implements ICredentialService {

    private final CredentialRepository credentialRepository;

    private final SearchRepository searchRepository;

    private final SecurityCheck securityCheck;

    @Override
    public Page<Credential> search(User user, Platform platform, Boolean valid, Pageable pageable) {
        return searchRepository.searchCredentials(user, platform, valid, pageable);
    }

    @Override
    public Credential get(String id) {
        return credentialRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Credential.class, id));
    }

    @Override
    public Credential create(User user, User issuer, Instant expiration, Instant notBefore) {
        ValidationUtils.notDeleted(user, "user");
        ValidationUtils.notBeforeNotAfterExpiration(expiration, notBefore);
        if (Role.SUPER.equals(user.getRole()) && expiration == null) {
            if (notBefore == null) {
                expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
            } else {
                expiration = notBefore.plus(30, ChronoUnit.MINUTES);
            }
        }
        Credential c = new Credential();
        c.setCreated(Instant.now());
        c.setCreator(issuer);
        c.setPlatform(user.getPlatform());
        c.setUser(user);
        c.setIssuer(issuer);
        c.setIssuedAt(Instant.now());
        c.setExpiration(expiration);
        c.setNotBefore(notBefore);
        c = credentialRepository.save(c);
        securityCheck.generateToken(c);
        return c;
    }

    @Override
    public Credential delete(Credential credential, User deleter) {
        return doDelete(credential, deleter);
    }

    @Override
    public void logout(Credential credential) {
        this.doDelete(credential, credential.getUser());
    }

    private Credential doDelete(Credential credential, User deleter) {
        ValidationUtils.notDeleted(credential, "entity");
        credential.setDeleted(Instant.now());
        credential.setDeleter(deleter);
        return credentialRepository.save(credential);
    }
}
