package com.fsquiroz.fsauth.security;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Role;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

import static java.util.Optional.ofNullable;

@RequiredArgsConstructor
public class RestAuthorizationFilter extends OncePerRequestFilter {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    private static final String PLATFORM_HEADER = "Platform-Id";

    private final SecurityCheck securityCheck;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        var platformId = request.getHeader(PLATFORM_HEADER);
        ofNullable(request.getHeader(AUTHORIZATION_HEADER))
                .map(this::parseToken)
                .ifPresent(token -> authenticate(token, platformId));
        chain.doFilter(request, response);
    }

    private String parseToken(String token) {
        if (token.startsWith("Bearer ")) {
            return token.substring(7);
        }
        return token;
    }

    private void authenticate(String token, String platformId) {
        try {
            Credential c = securityCheck.authenticate(token);
            if (canAuthenticate(c, platformId)) {
                var auth = new UsernamePasswordAuthenticationToken(c, null, c.getAuthorities());
                SecurityContextHolder.getContext().setAuthentication(auth);
            }
        } catch (Exception ignored) {
        }
    }

    private boolean canAuthenticate(Credential credential, String platformId) {
        return ofNullable(credential)
                .map(c -> Role.SUPER.equals(c.getUser().getRole()) || c.getPlatform().getId().equals(platformId))
                .orElse(false);
    }
}
