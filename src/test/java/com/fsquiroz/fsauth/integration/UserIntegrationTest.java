package com.fsquiroz.fsauth.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MResetPassword;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.mock.CredentialMock;
import com.fsquiroz.fsauth.mock.PlatformMock;
import com.fsquiroz.fsauth.mock.TokenMock;
import com.fsquiroz.fsauth.mock.UserMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.TreeMap;

import static org.hamcrest.Matchers.hasSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class UserIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PlatformMock platformMock;

    @Autowired
    private UserMock userMock;

    @Autowired
    private CredentialMock credentialMock;

    @Autowired
    private TokenMock tokenMock;

    private Platform p1;

    private Platform p2;

    private User root;

    private User admin1;

    private User admin2;

    private User client1;

    private User client2;

    private User user1;

    private Credential asRoot;

    private Credential asAdmin1;

    private Credential asAdmin2;

    private Credential asClient1;

    private Credential asClient2;

    private Credential asUser1;

    private Token forRoot;

    private Token forAdmin1;

    private Token forAdmin2;

    private Token forClient1;

    private Token forClient2;

    private Token forUser1;

    @BeforeEach
    public void setup() {
        p1 = platformMock.create("Abc", null);
        p2 = platformMock.create("Zyx", null);
        root = userMock.create("root", null, Role.SUPER, null);
        admin1 = userMock.create("admin1", null, Role.PLATFORM_ADMIN, p1);
        admin2 = userMock.create("admin2", null, Role.PLATFORM_ADMIN, p2);
        client1 = userMock.create("client1", null, Role.PLATFORM_CLIENT, p1);
        client2 = userMock.create("client2", null, Role.PLATFORM_CLIENT, p2);
        user1 = userMock.create("user1", null, Role.PLATFORM_USER, p1);
        asRoot = credentialMock.create(root, null, null, null);
        asAdmin1 = credentialMock.create(admin1, root, null, null);
        asAdmin2 = credentialMock.create(admin2, root, null, null);
        asClient1 = credentialMock.create(client1, root, null, null);
        asClient2 = credentialMock.create(client2, root, null, null);
        asUser1 = credentialMock.create(user1, client1, null, null);
        forRoot = tokenMock.create(root, Token.Type.RESET_PASSWORD);
        forAdmin1 = tokenMock.create(admin1, Token.Type.RESET_PASSWORD);
        forAdmin2 = tokenMock.create(admin2, Token.Type.RESET_PASSWORD);
        forClient1 = tokenMock.create(client1, Token.Type.RESET_PASSWORD);
        forClient2 = tokenMock.create(client2, Token.Type.RESET_PASSWORD);
        forUser1 = tokenMock.create(user1, Token.Type.RESET_PASSWORD);
    }

    @AfterEach
    public void cleanup() {
        tokenMock.cleanup();
        credentialMock.cleanup();
        userMock.cleanup();
        platformMock.cleanup();
    }

    @Test
    public void searchAsSuper() throws Exception {
        runSearch(asRoot, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(6)));
    }

    @Test
    public void searchAsSuperForPlatform() throws Exception {
        runSearch(asRoot, p1, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(3)));
    }

    @Test
    public void searchAsAdmin1() throws Exception {
        runSearch(asAdmin1, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(3)));
    }

    @Test
    public void searchAsAdmin2() throws Exception {
        runSearch(asAdmin2, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(2)));
    }

    @Test
    public void searchAsClient1() throws Exception {
        runSearch(asClient1, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsClient2() throws Exception {
        runSearch(asClient2, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsUser1() throws Exception {
        runSearch(asUser1, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        runSearch(null, null, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asRoot, mu, p1, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin1() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asAdmin1, mu, null, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin2() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asAdmin2, mu, null, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient1() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asClient1, mu, null, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsClient2() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asClient2, mu, null, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsUser1() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(asUser1, mu, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        MUser mu = new MUser();
        mu.setPlatformIdentifier("new-identifier");
        mu.setRole(Role.PLATFORM_USER);
        runCreate(null, mu, null, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        runGet(asRoot, user1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin1() throws Exception {
        runGet(asAdmin1, user1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin2() throws Exception {
        runGet(asAdmin2, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsClient1() throws Exception {
        runGet(asClient1, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsClient2() throws Exception {
        runGet(asClient1, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsUser1() throws Exception {
        runGet(asUser1, user1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        runGet(null, user1, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsSuper() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asRoot, user1, mu, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsAdmin1() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asAdmin1, user1, mu, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsAdmin2() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asAdmin2, user1, mu, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsClient1() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asClient1, user1, mu, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsClient2() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asClient2, user1, mu, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsUser1() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(asUser1, user1, mu, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        MUser mu = new MUser();
        mu.setAttributes(new TreeMap<>());
        runUpdate(null, user1, mu, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteAsSuper() throws Exception {
        runDelete(asRoot, user1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin1() throws Exception {
        runDelete(asAdmin1, user1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin2() throws Exception {
        runDelete(asAdmin2, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient1() throws Exception {
        runDelete(asClient1, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient2() throws Exception {
        runDelete(asClient2, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsUser1() throws Exception {
        runDelete(asUser1, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        runDelete(null, user1, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void changePasswordAsRoot() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asRoot, user1, mrp, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void changePasswordAsAdmin1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asAdmin1, user1, mrp, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void changePasswordAsAdmin2() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asAdmin2, user1, mrp, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void changePasswordAsClient1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asClient1, user1, mrp, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void changePasswordAsClient2() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asClient2, user1, mrp, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void changePasswordAsUser1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(asUser1, user1, mrp, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void changePasswordAsAnonymous() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setOldPassword(user1.getPassword());
        mrp.setNewPassword("new-password");
        runChangePassword(null, user1, mrp, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void resetPasswordStep1AsRoot() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asRoot, user1, p1, false, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPasswordStep1AsRootWithMissingParams() throws Exception {
        runResetPassword(asRoot, user1, p1, true, MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void resetPasswordStep1AsAdmin1() throws Exception {
        runResetPassword(asAdmin1, user1, null, false, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPasswordStep1AsAdmin2() throws Exception {
        runResetPassword(asAdmin2, user1, null, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep1AsClient1() throws Exception {
        runResetPassword(asClient1, user1, null, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep1AsClient2() throws Exception {
        runResetPassword(asClient2, user1, null, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep1AsUser1() throws Exception {
        runResetPassword(asUser1, user1, null, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep1AsAnonymous() throws Exception {
        runResetPassword(null, user1, null, false, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void resetPasswordStep2AsRoot() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asRoot, user1, p1, mrp, false, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPasswordStep2AsRootWithMissingParams() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asRoot, user1, p1, mrp, true, MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void resetPasswordStep2AsAdmin1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asAdmin1, user1, null, mrp, false, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPasswordStep2AsAdmin2() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asAdmin2, user1, null, mrp, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep2AsClient1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asClient1, user1, null, mrp, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep2AsClient2() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asClient2, user1, null, mrp, false, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void resetPasswordStep2AsUser1() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(asUser1, user1, null, mrp, false, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void resetPasswordStep2AsAnonymous() throws Exception {
        MResetPassword mrp = new MResetPassword();
        mrp.setToken(forUser1.getId());
        mrp.setNewPassword("new-password");
        runResetPassword(null, user1, null, mrp, false, MockMvcResultMatchers.status().isUnauthorized());
    }

    private ResultActions runSearch(Credential as, Platform platform, ResultMatcher expect) throws Exception {
        log.info(
                "Test search users as {} for platform {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous",
                platform != null ? platform.getName() : "no-platform"
        );
        return mockMvc.perform(search(as, platform))
                .andExpect(expect);
    }

    private ResultActions runCreate(Credential as, MUser user, Platform platform, ResultMatcher expect) throws Exception {
        log.info(
                "Test create user as {} for platform {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous",
                platform != null ? platform.getName() : "no-platform"
        );
        return mockMvc.perform(create(as, user, platform))
                .andExpect(expect);
    }

    private ResultActions runGet(Credential as, User user, ResultMatcher expect) throws Exception {
        log.info(
                "Test get user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(get(as, user))
                .andExpect(expect);
    }

    private ResultActions runUpdate(Credential as, User user, MUser edit, ResultMatcher expect) throws Exception {
        log.info(
                "Test update user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(update(as, user, edit))
                .andExpect(expect);
    }

    private ResultActions runDelete(Credential as, User user, ResultMatcher expect) throws Exception {
        log.info(
                "Test delete user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(delete(as, user))
                .andExpect(expect);
    }

    private ResultActions runChangePassword(Credential as, User user, MResetPassword resetPassword, ResultMatcher expect) throws Exception {
        log.info(
                "Test change password from user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(changePassword(as, user, resetPassword))
                .andExpect(expect);
    }

    private ResultActions runResetPassword(Credential as, User user, Platform platform, boolean ignoreUser, ResultMatcher expect) throws Exception {
        log.info(
                "Test reset password step 1 from user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(resetPassword(as, user, platform, ignoreUser))
                .andExpect(expect);
    }

    private ResultActions runResetPassword(Credential as, User user, Platform platform, MResetPassword resetPassword, boolean ignoreUser, ResultMatcher expect) throws Exception {
        log.info(
                "Test reset password step 2 from user as {}",
                as != null && as.getUser() != null ? as.getUser().getRole() : "anonymous"
        );
        return mockMvc.perform(resetPassword(as, user, platform, resetPassword, ignoreUser))
                .andExpect(expect);
    }

    private MockHttpServletRequestBuilder search(Credential who, Platform platform) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users")
                .accept(MediaType.APPLICATION_JSON);
        if (platform != null) {
            request.param("platformId", platform.getId());
        }
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(Credential who, MUser user, Platform platform) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(user));
        if (platform != null) {
            request.param("platformId", platform.getId());
        }
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(Credential who, User toGet) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users/{userId}", toGet.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder update(Credential who, User toUpdate, MUser edit) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/users/{userId}", toUpdate.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edit));
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(Credential who, User toDelete) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/users/{userId}", toDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder changePassword(Credential who, User toUpdate, MResetPassword resetPassword) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users/{userId}/password", toUpdate.getId())
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(resetPassword));
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder resetPassword(Credential who, User toReset, Platform platform, boolean ignoreUser) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/users/password")
                .accept(MediaType.APPLICATION_JSON);
        if (!ignoreUser) {
            if (platform != null) {
                request.param("platformId", platform.getId())
                        .param("platformIdentifier", toReset.getPlatformIdentifier());
            } else {
                request.param("userId", toReset.getId());
            }
        }
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder resetPassword(Credential who, User toReset, Platform platform, MResetPassword resetPassword, boolean ignoreUser) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/users/password")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(resetPassword));
        if (!ignoreUser) {
            if (platform != null) {
                request.param("platformId", platform.getId())
                        .param("platformIdentifier", toReset.getPlatformIdentifier());
            } else {
                request.param("userId", toReset.getId());
            }
        }
        credentialMock.authenticate(request, who);
        return request;
    }

}
