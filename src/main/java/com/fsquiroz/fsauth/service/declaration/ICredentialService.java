package com.fsquiroz.fsauth.service.declaration;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;

public interface ICredentialService {

    @PreAuthorize("hasAnyRole('SUPER', 'PLATFORM_ADMIN')")
    Page<Credential> search(User user, Platform platform, Boolean valid, Pageable pageable);

    @PostAuthorize("@securityCheck.canUseCredential(returnObject)")
    Credential get(String id);

    Credential create(User user, User issuer, Instant expiration, Instant notBefore);

    @PreAuthorize("@securityCheck.canDeleteCredential(#credential)")
    Credential delete(Credential credential, User deleter);

    @PostAuthorize("@securityCheck.canUseCredential(#credential)")
    void logout(Credential credential);

}
