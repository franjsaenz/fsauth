package com.fsquiroz.fsauth.controller;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MResetPassword;
import com.fsquiroz.fsauth.entity.json.MResponse;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.exception.BadRequestException;
import com.fsquiroz.fsauth.security.SecurityCheck;
import com.fsquiroz.fsauth.service.declaration.IBuildService;
import com.fsquiroz.fsauth.service.declaration.IPlatformService;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class UserController {

    private final IUserService userService;

    private final IPlatformService platformService;

    private final IBuildService buildService;

    private final SecurityCheck securityCheck;

    @GetMapping
    public ResponseEntity<?> search(
            @RequestParam(required = false) String term,
            @RequestParam(required = false) String platformId,
            @RequestParam(required = false) List<Role> roles,
            @RequestParam(required = false, defaultValue = "false") boolean withDeleted,
            Pageable pageable
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        Platform platform = getPlatform(loggedIn, platformId);
        Page<User> users = userService.search(term, platform, roles, withDeleted, pageable);
        return ResponseEntity.ok(buildService.users(users));
    }

    @PostMapping
    public ResponseEntity<?> create(
            @RequestParam(required = false) String platformId,
            @RequestBody MUser user
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        Platform platform = getPlatform(loggedIn, platformId);
        User u = userService.create(user, loggedIn, platform);
        return new ResponseEntity<>(buildService.user(u), HttpStatus.CREATED);
    }

    @GetMapping("/{userId}")
    public ResponseEntity<?> get(@PathVariable String userId) {
        User user = userService.get(userId);
        return ResponseEntity.ok(buildService.user(user));
    }

    @PutMapping("/{userId}")
    public ResponseEntity<?> update(
            @PathVariable String userId,
            @RequestBody MUser user
    ) {
        User updater = securityCheck.getAuthenticated();
        User u = userService.get(userId);
        u = userService.update(u, user, updater);
        return ResponseEntity.ok(buildService.user(u));
    }

    @DeleteMapping("/{userId}")
    public ResponseEntity<?> delete(@PathVariable String userId) {
        User deleter = securityCheck.getAuthenticated();
        User user = userService.get(userId);
        userService.delete(user, deleter);
        return ResponseEntity.ok(MResponse.of("User deleted"));
    }

    @PostMapping("/{userId}/password")
    public ResponseEntity<?> updatePassword(
            @PathVariable String userId,
            @RequestBody MResetPassword resetPassword
    ) {
        User user = userService.get(userId);
        userService.changePassword(user, resetPassword);
        return ResponseEntity.ok(MResponse.of("Password updated"));
    }

    @GetMapping("/password")
    public ResponseEntity<?> resetPassword(
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) String platformIdentifier,
            @RequestParam(required = false) String platformId
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        Platform platform = getPlatform(loggedIn, platformId);
        User user = getUser(platform, userId, platformIdentifier);
        Credential token = userService.resetPassword(user, loggedIn);
        return ResponseEntity.ok(buildService.credential(token));
    }

    @PostMapping("/password")
    public ResponseEntity<?> resetPassword(
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) String platformIdentifier,
            @RequestParam(required = false) String platformId,
            @RequestBody MResetPassword resetPassword
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        Platform platform = getPlatform(loggedIn, platformId);
        User user = getUser(platform, userId, platformIdentifier);
        userService.resetPassword(user, resetPassword, loggedIn);
        return ResponseEntity.ok(MResponse.of("Password reset"));
    }

    private Platform getPlatform(User loggedIn, String platformId) {
        Platform platform = null;
        if (Role.SUPER.equals(loggedIn.getRole())) {
            if (platformId != null) {
                platform = platformService.get(platformId);
            }
        } else {
            platform = loggedIn.getPlatform();
        }
        return platform;
    }

    private User getUser(Platform platform, String userId, String platformIdentifier) {
        User user;
        if (userId != null) {
            user = userService.get(userId);
        } else if (platformIdentifier != null && platform != null) {
            user = userService.get(platformIdentifier, platform);
        } else {
            throw BadRequestException.byMissingParam(platform, userId, platformIdentifier);
        }
        return user;
    }

}
