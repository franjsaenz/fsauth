package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Role {

    SUPER("SUPER"),
    PLATFORM_ADMIN("PLATFORM_ADMIN"),
    PLATFORM_USER("PLATFORM_USER"),
    PLATFORM_CLIENT("PLATFORM_CLIENT");

    private final String name;
}
