package com.fsquiroz.fsauth.service.declaration;

import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MLogin;
import com.fsquiroz.fsauth.entity.json.MResetPassword;
import com.fsquiroz.fsauth.entity.json.MUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

import java.time.Instant;
import java.util.List;

public interface IUserService {

    Credential login(MLogin login, LoginMethod method, User issuer, Instant expiration, Instant notBefore);

    @PreAuthorize("hasAnyRole('SUPER', 'PLATFORM_ADMIN')")
    Page<User> search(String term, Platform platform, List<Role> roles, boolean withDeleted, Pageable pageable);

    @PostAuthorize("@securityCheck.canGetUser(returnObject)")
    User get(String id);

    @PostAuthorize("@securityCheck.canGetUser(returnObject)")
    User get(String platformIdentifier, Platform platform);

    @PreAuthorize("hasAnyRole('SUPER', 'PLATFORM_ADMIN', 'PLATFORM_CLIENT')")
    User create(MUser model, User creator, Platform platform);

    @PreAuthorize("@securityCheck.canUpdateUser(#original)")
    User update(User original, MUser edit, User updater);

    @PreAuthorize("hasAnyRole('SUPER', 'PLATFORM_ADMIN') and @securityCheck.canUpdateUser(#original)")
    User delete(User original, User deleter);

    @PreAuthorize("@securityCheck.canUpdateUser(#original)")
    User changePassword(User original, MResetPassword resetPassword);

    @PreAuthorize("@securityCheck.canResetPassword(#original)")
    Credential resetPassword(User original, User issuer);

    User resetPassword(User original, MResetPassword resetPassword, User issuer);

}
