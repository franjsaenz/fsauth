package com.fsquiroz.fsauth.config;

import com.fsquiroz.fsauth.entity.json.MStatus;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.springframework.boot.actuate.web.exchanges.HttpExchangeRepository;
import org.springframework.boot.actuate.web.exchanges.InMemoryHttpExchangeRepository;
import org.springframework.boot.info.BuildProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.*;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;
import java.util.Optional;

import static org.springframework.data.web.config.EnableSpringDataWebSupport.PageSerializationMode.VIA_DTO;

@Configuration
@Profile("!test")
@PropertySources({
        @PropertySource(value = "classpath:application.properties"),
        @PropertySource(value = "file:${user.home}${file.separator}.fsauth${file.separator}application.properties")
})
@EnableSpringDataWebSupport(pageSerializationMode = VIA_DTO)
public class Beans {

    @Bean
    public MStatus status(Optional<BuildProperties> buildProperties) {
        var startUp = Instant.now();
        return buildProperties
                .map(props -> new MStatus(props.getName(), props.getVersion(), props.getTime(), startUp, "Up"))
                .orElse(new MStatus("fsAuth", "DEVELOPMENT", startUp, startUp, "Up"));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            private final StrongPasswordEncryptor spe = new StrongPasswordEncryptor();

            @Override
            public String encode(CharSequence rawPassword) {
                return spe.encryptPassword(rawPassword.toString());
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return spe.checkPassword(rawPassword.toString(), encodedPassword);
            }
        };
    }

    @Bean
    public ServletRegistrationBean<LoggerDispatcherServlet> dispatcherRegistration(
            LoggerDispatcherServlet loggerDispatcherServlet) {
        return new ServletRegistrationBean<>(loggerDispatcherServlet);
    }

    @Bean
    public HttpExchangeRepository httpExchangeRepository() {
        var repo = new InMemoryHttpExchangeRepository();
        repo.setCapacity(500);
        return repo;
    }

}
