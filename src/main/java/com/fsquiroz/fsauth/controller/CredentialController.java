package com.fsquiroz.fsauth.controller;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MResponse;
import com.fsquiroz.fsauth.security.SecurityCheck;
import com.fsquiroz.fsauth.service.declaration.IBuildService;
import com.fsquiroz.fsauth.service.declaration.ICredentialService;
import com.fsquiroz.fsauth.service.declaration.IPlatformService;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@RestController
@RequestMapping("/credentials")
@RequiredArgsConstructor
public class CredentialController {

    private final ICredentialService credentialService;

    private final IUserService userService;

    private final IPlatformService platformService;

    private final IBuildService buildService;

    private final SecurityCheck securityCheck;

    @GetMapping
    public ResponseEntity<?> search(
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) String platformId,
            @RequestParam(required = false) Boolean valid,
            Pageable pageable
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        Platform platform = getPlatform(loggedIn, platformId);
        User user = null;
        if (userId != null) {
            user = userService.get(userId);
        }
        Page<Credential> credentials = credentialService.search(user, platform, valid, pageable);
        return ResponseEntity.ok(buildService.credentials(credentials));
    }

    @PostMapping
    public ResponseEntity<?> create(
            @RequestParam(required = false) String userId,
            @RequestParam(required = false) Instant expiration,
            @RequestParam(required = false) Instant notBefore
    ) {
        User loggedIn = securityCheck.getAuthenticated();
        User user = null;
        if (userId != null) {
            user = userService.get(userId);
        }
        Credential credential = credentialService.create(
                user,
                loggedIn,
                expiration,
                notBefore
        );
        return new ResponseEntity<>(buildService.credential(credential), HttpStatus.CREATED);
    }

    @GetMapping("/{credentialId}")
    public ResponseEntity<?> get(@PathVariable String credentialId) {
        Credential credential = credentialService.get(credentialId);
        return ResponseEntity.ok(buildService.credential(credential));
    }

    @DeleteMapping("/{credentialId}")
    public ResponseEntity<?> delete(@PathVariable String credentialId) {
        User loggedIn = securityCheck.getAuthenticated();
        Credential credential = credentialService.get(credentialId);
        credentialService.delete(credential, loggedIn);
        return ResponseEntity.ok(MResponse.of("Credential deleted"));
    }

    private Platform getPlatform(User loggedIn, String platformId) {
        Platform platform = null;
        if (Role.SUPER.equals(loggedIn.getRole())) {
            if (platformId != null) {
                platform = platformService.get(platformId);
            }
        } else {
            platform = loggedIn.getPlatform();
        }
        return platform;
    }

}
