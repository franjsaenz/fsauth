package com.fsquiroz.fsauth.service;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TimeUtils {

    private static final DateTimeFormatter STANDARD = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    public static String standard(Instant i) {
        return STANDARD.format(ZonedDateTime.ofInstant(i, ZoneOffset.UTC));
    }

    public static Instant standard(String s) {
        ZonedDateTime zdt = ZonedDateTime.parse(s, STANDARD);
        return zdt.toInstant();
    }

}
