package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum LoginMethod {

    PASSWORD("PASSWORD"),
    PASSWORDLESS("PASSWORDLESS");

    private final String name;

}
