package com.fsquiroz.fsauth.config;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.security.SecurityCheck;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.HandlerExecutionChain;

import java.time.Instant;

@Component
@Slf4j
@RequiredArgsConstructor
public class LoggerDispatcherServlet extends DispatcherServlet {

    private final SecurityCheck securityCheck;

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        long start = System.currentTimeMillis();
        HandlerExecutionChain handler = getHandler(request);
        try {
            super.doDispatch(request, response);
        } finally {
            if (handler != null && handler.toString().contains("fsquiroz")) {
                long end = System.currentTimeMillis();
                debug(request, response, handler, end - start);
            }
        }
    }

    private void debug(HttpServletRequest request,
                       HttpServletResponse response,
                       HandlerExecutionChain handler,
                       long elapsed) {
        if (log.isInfoEnabled()) {
            Credential c = securityCheck.getAuthentication().orElse(null);
            User u = c != null ? c.getUser() : null;
            Platform p = u != null ? u.getPlatform() : null;
            String credential;
            Instant issueAt = null;
            Instant expiration = null;
            Instant notBefore = null;
            String user;
            String role;
            String platformId;
            String platformName;
            if (c != null) {
                credential = c.getId();
                issueAt = c.getIssuedAt();
                expiration = c.getExpiration();
                notBefore = c.getNotBefore();
            } else {
                credential = "ANONYMOUS";
            }
            if (u != null) {
                user = u.getPlatformIdentifier();
            } else {
                user = "ANONYMOUS";
            }
            if (u != null && u.getRole() != null) {
                role = u.getRole().getName();
            } else {
                role = "ANONYMOUS";
            }
            if (p != null) {
                platformId = p.getId();
                platformName = p.getName();
            } else {
                platformId = platformName = null;
            }
            log.info("\n"
                            + "HTTP Request:\n"
                            + "\turl: {}\n"
                            + "\tmethod: {}\n"
                            + "\tclient: {}\n"
                            + "\tcredentialId: {}\n"
                            + "\tissueAt: {}\n"
                            + "\texpiration: {}\n"
                            + "\tnotBefore: {}\n"
                            + "\tuserId: {}\n"
                            + "\tuserPlatformIdentifier: {}\n"
                            + "\tuserRole: {}\n"
                            + "\tplatformId: {}\n"
                            + "\tplatformName: {}\n"
                            + "Handler:\n"
                            + "\tmethod: {}\n"
                            + "\ttook: {}ms\n"
                            + "HTTP Response:\n"
                            + "\tstatus: {}\n",
                    request.getRequestURL(),
                    request.getMethod(),
                    request.getRemoteAddr(),
                    credential,
                    issueAt,
                    expiration,
                    notBefore,
                    u != null ? u.getId() : "ANONYMOUS",
                    user,
                    role,
                    platformId,
                    platformName,
                    handler.toString(),
                    elapsed,
                    response.getStatus());
        }
    }

}
