package com.fsquiroz.fsauth.runner;

import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.mongodb.client.result.UpdateResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Profile("!test")
@RequiredArgsConstructor
public class PasswordTypeSetterRunner implements ApplicationRunner {

    private final MongoOperations dbOperations;

    @Override
    public void run(ApplicationArguments args) {
        if (existsClientsWithoutNotSetPasswordType()) {
            setNotSetPasswordType();
        }
        if (existsUsersWithoutPasswordType()) {
            setPasswordType();
        }
    }

    private boolean existsClientsWithoutNotSetPasswordType() {
        return dbOperations.exists(whereUserIsClientAndPasswordTypeIsNotNotSet(), User.class);
    }

    private boolean existsUsersWithoutPasswordType() {
        return dbOperations.exists(wherePasswordTypeIsNull(), User.class);
    }

    private void setNotSetPasswordType() {
        log.debug("Updating Clients without Not Set password type");
        var result = dbOperations.updateMulti(
                whereUserIsClientAndPasswordTypeIsNotNotSet(),
                Update.update("passwordType", User.PasswordType.NOT_SET),
                User.class
        );
        logResult(result, User.PasswordType.NOT_SET);
    }

    private void setPasswordType() {
        log.debug("Updating User with unset password type");
        var result = dbOperations.updateMulti(
                wherePasswordTypeIsNull(),
                Update.update("passwordType", User.PasswordType.V_1),
                User.class
        );
        logResult(result, User.PasswordType.V_1);
    }

    private void logResult(UpdateResult result, User.PasswordType passwordType) {
        if (result.getModifiedCount() == result.getMatchedCount()) {
            log.info("{} users updated with Password Type {}", result.getModifiedCount(), passwordType);
        } else {
            log.warn("{} of {} users were updated with Password Type {}. {} missing updates",
                    result.getModifiedCount(),
                    result.getMatchedCount(),
                    passwordType,
                    result.getMatchedCount() - result.getModifiedCount());
        }
    }

    private Query wherePasswordTypeIsNull() {
        return new Query().addCriteria(Criteria.where("passwordType").isNull());
    }

    private Query whereUserIsClientAndPasswordTypeIsNotNotSet() {
        return new Query()
                .addCriteria(Criteria.where("role").is(Role.PLATFORM_CLIENT))
                .addCriteria(Criteria.where("passwordType").ne(User.PasswordType.NOT_SET));
    }
}
