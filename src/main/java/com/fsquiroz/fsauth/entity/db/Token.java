package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.Instant;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Token extends Filterable {

    private Type type;

    private Instant expiration;

    @DBRef
    private User user;

    public enum Type {
        LOGIN, RESET_PASSWORD
    }

}
