package com.fsquiroz.fsauth.integration;

import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.mock.CredentialMock;
import com.fsquiroz.fsauth.mock.PlatformMock;
import com.fsquiroz.fsauth.mock.UserMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.Matchers.hasSize;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class CredentialIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PlatformMock platformMock;

    @Autowired
    private UserMock userMock;

    @Autowired
    private CredentialMock credentialMock;

    private Platform p1;

    private Platform p2;

    private User root;

    private User admin1;

    private User admin2;

    private User client1;

    private User client2;

    private User user1;

    private Credential asRoot;

    private Credential asAdmin1;

    private Credential asAdmin2;

    private Credential asClient1;

    private Credential asClient2;

    private Credential asUser1;

    @BeforeEach
    public void setup() {
        p1 = platformMock.create("Abc", null);
        p2 = platformMock.create("Zyx", null);
        root = userMock.create("root", null, Role.SUPER, null);
        admin1 = userMock.create("admin1", null, Role.PLATFORM_ADMIN, p1);
        admin2 = userMock.create("admin2", null, Role.PLATFORM_ADMIN, p2);
        client1 = userMock.create("client1", null, Role.PLATFORM_CLIENT, p1);
        client2 = userMock.create("client2", null, Role.PLATFORM_CLIENT, p2);
        user1 = userMock.create("user1", null, Role.PLATFORM_USER, p1);
        asRoot = credentialMock.create(root, null, null, null);
        asAdmin1 = credentialMock.create(admin1, root, null, null);
        asAdmin2 = credentialMock.create(admin2, root, null, null);
        asClient1 = credentialMock.create(client1, root, null, null);
        asClient2 = credentialMock.create(client2, root, null, null);
        asUser1 = credentialMock.create(user1, client1, null, null);
    }

    @AfterEach
    public void cleanup() {
        credentialMock.cleanup();
        userMock.cleanup();
        platformMock.cleanup();
    }

    @Test
    public void searchAsSuper() throws Exception {
        runSearch(asRoot, null, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(6)));
    }

    @Test
    public void searchAsSuperForPlatform() throws Exception {
        runSearch(asRoot, null, p1, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(3)));
    }

    @Test
    public void searchAsSuperForUser() throws Exception {
        runSearch(asRoot, user1, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(1)));
    }

    @Test
    public void searchAsAdmin1() throws Exception {
        runSearch(asAdmin1, null, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(3)));
    }

    @Test
    public void searchAsAdmin2() throws Exception {
        runSearch(asAdmin2, null, null, MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.content", hasSize(2)));
    }

    @Test
    public void searchAsClient1() throws Exception {
        runSearch(asClient1, null, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsClient2() throws Exception {
        runSearch(asClient2, null, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsUser1() throws Exception {
        runSearch(asUser1, null, null, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        runSearch(null, null, null, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        runCreate(asRoot, user1, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin1() throws Exception {
        runCreate(asAdmin1, user1, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin2() throws Exception {
        runCreate(asAdmin2, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient1() throws Exception {
        runCreate(asClient1, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient2() throws Exception {
        runCreate(asClient2, user1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsUser1() throws Exception {
        runCreate(asUser1, user1, MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        runCreate(null, user1, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        runGet(asRoot, asUser1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin1() throws Exception {
        runGet(asAdmin1, asUser1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAdmin2() throws Exception {
        runGet(asAdmin2, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsClient1() throws Exception {
        runGet(asClient1, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsClient2() throws Exception {
        runGet(asClient2, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsUser1() throws Exception {
        runGet(asUser1, asUser1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        runGet(null, asUser1, MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        runDelete(asRoot, asUser1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin1() throws Exception {
        runDelete(asAdmin1, asUser1, MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin2() throws Exception {
        runDelete(asAdmin2, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient1() throws Exception {
        runDelete(asClient1, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient2() throws Exception {
        runDelete(asClient2, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsUser1() throws Exception {
        runDelete(asUser1, asUser1, MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        runDelete(null, asUser1, MockMvcResultMatchers.status().isUnauthorized());
    }

    private ResultActions runSearch(Credential as, User user, Platform platform, ResultMatcher expect) throws Exception {
        log.info(
                "Test search credentials as {} for platform {}",
                as != null && as.getUser() != null ? as.getUser().getPlatformIdentifier() : "anonymous",
                platform != null ? platform.getName() : "no-platform"
        );
        return mockMvc.perform(search(as, user, platform))
                .andExpect(expect);
    }

    private ResultActions runCreate(Credential as, User user, ResultMatcher expect) throws Exception {
        log.info(
                "Test create credential as {}",
                as != null && as.getUser() != null ? as.getUser().getPlatformIdentifier() : "anonymous"
        );
        return mockMvc.perform(create(as, user))
                .andExpect(expect);
    }

    private ResultActions runGet(Credential as, Credential credential, ResultMatcher expect) throws Exception {
        log.info(
                "Test get credential as {}",
                as != null && as.getUser() != null ? as.getUser().getPlatformIdentifier() : "anonymous"
        );
        return mockMvc.perform(get(as, credential))
                .andExpect(expect);
    }

    private ResultActions runDelete(Credential as, Credential credential, ResultMatcher expect) throws Exception {
        log.info(
                "Test delete credential as {}",
                as != null && as.getUser() != null ? as.getUser().getPlatformIdentifier() : "anonymous"
        );
        return mockMvc.perform(delete(as, credential))
                .andExpect(expect);
    }

    private MockHttpServletRequestBuilder search(Credential who, User user, Platform platform) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/credentials")
                .accept(MediaType.APPLICATION_JSON);
        if (user != null) {
            request.param("userId", user.getId());
        }
        if (platform != null) {
            request.param("platformId", platform.getId());
        }
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(Credential who, User user) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/credentials")
                .accept(MediaType.APPLICATION_JSON);
        if (user != null) {
            request.param("userId", user.getId());
        }
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(Credential who, Credential toGet) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/credentials/{credentialId}", toGet.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(Credential who, Credential toDelete) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/credentials/{credentialId}", toDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

}
