package com.fsquiroz.fsauth.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MCreatePlatform;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.mock.CredentialMock;
import com.fsquiroz.fsauth.mock.PlatformMock;
import com.fsquiroz.fsauth.mock.UserMock;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@ActiveProfiles("test")
@Slf4j
public class PlatformIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Autowired
    private PlatformMock platformMock;

    @Autowired
    private UserMock userMock;

    @Autowired
    private CredentialMock credentialMock;

    private Platform p1;

    private Platform p2;

    private Credential asRoot;

    private Credential asAdmin1;

    private Credential asAdmin2;

    private Credential asClient1;

    private Credential asClient2;

    private Credential asUser1;

    @BeforeEach
    public void setup() {
        p1 = platformMock.create(null, null);
        p2 = platformMock.create(null, null);
        User root = userMock.create("root", null, Role.SUPER, null);
        User admin1 = userMock.create("admin1", null, Role.PLATFORM_ADMIN, p1);
        User admin2 = userMock.create("admin2", null, Role.PLATFORM_ADMIN, p2);
        User client1 = userMock.create("client1", null, Role.PLATFORM_CLIENT, p1);
        User client2 = userMock.create("client2", null, Role.PLATFORM_CLIENT, p2);
        User user1 = userMock.create("user1", null, Role.PLATFORM_USER, p1);
        asRoot = credentialMock.create(root, null, null, null);
        asAdmin1 = credentialMock.create(admin1, root, null, null);
        asAdmin2 = credentialMock.create(admin2, root, null, null);
        asClient1 = credentialMock.create(client1, root, null, null);
        asClient2 = credentialMock.create(client2, root, null, null);
        asUser1 = credentialMock.create(user1, client1, null, null);
    }

    @AfterEach
    public void cleanup() {
        credentialMock.cleanup();
        userMock.cleanup();
        platformMock.cleanup();
    }

    @Test
    public void searchAsRoot() throws Exception {
        log.info("Test search platforms as root");
        mockMvc.perform(search(asRoot))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void searchAsAdmin() throws Exception {
        log.info("Test search platforms as admin");
        mockMvc.perform(search(asAdmin1))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsClient() throws Exception {
        log.info("Test search platforms as client");
        mockMvc.perform(search(asClient1))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsUser() throws Exception {
        log.info("Test search platforms as user");
        mockMvc.perform(search(asUser1))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void searchAsAnonymous() throws Exception {
        log.info("Test search platforms as anonymous");
        mockMvc.perform(search(null))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void createAsRoot() throws Exception {
        log.info("Test create platform as root");
        Credential as = asRoot;
        MCreatePlatform mcp = new MCreatePlatform();
        mcp.setName("Platform 3");
        mcp.setDescription("Description 3");
        mcp.setPlatformIdentifier("platform-3");
        mcp.setPassword("password");
        mockMvc.perform(create(mcp, as))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void createAsAdmin() throws Exception {
        log.info("Test create platform as admin");
        Credential as = asAdmin1;
        MCreatePlatform mcp = new MCreatePlatform();
        mcp.setName("Platform 3");
        mcp.setDescription("Description 3");
        mcp.setPlatformIdentifier("platform-3");
        mcp.setPassword("password");
        mockMvc.perform(create(mcp, as))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsClient() throws Exception {
        log.info("Test create platform as client");
        Credential as = asClient1;
        MCreatePlatform mcp = new MCreatePlatform();
        mcp.setName("Platform 3");
        mcp.setDescription("Description 3");
        mcp.setPlatformIdentifier("platform-3");
        mcp.setPassword("password");
        mockMvc.perform(create(mcp, as))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsUser() throws Exception {
        log.info("Test create platform as user");
        Credential as = asUser1;
        MCreatePlatform mcp = new MCreatePlatform();
        mcp.setName("Platform 3");
        mcp.setDescription("Description 3");
        mcp.setPlatformIdentifier("platform-3");
        mcp.setPassword("password");
        mockMvc.perform(create(mcp, as))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void createAsAnonymous() throws Exception {
        log.info("Test create platform as anonymous");
        Credential as = null;
        MCreatePlatform mcp = new MCreatePlatform();
        mcp.setName("Platform 3");
        mcp.setDescription("Description 3");
        mcp.setPlatformIdentifier("platform-3");
        mcp.setPassword("password");
        mockMvc.perform(create(mcp, as))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void getAsRoot() throws Exception {
        log.info("Test get platform as super");
        Credential as = asRoot;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsSelfAdmin() throws Exception {
        log.info("Test get platform as self admin");
        Credential as = asAdmin1;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsSelfClient() throws Exception {
        log.info("Test get platform as self client");
        Credential as = asClient1;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsSelfUser() throws Exception {
        log.info("Test get platform as self user");
        Credential as = asUser1;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void getAsOtherAdmin() throws Exception {
        log.info("Test get platform as other admin");
        Credential as = asAdmin2;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsOtherClient() throws Exception {
        log.info("Test get platform as other client");
        Credential as = asClient2;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void getAsAnonymous() throws Exception {
        log.info("Test get platform as anonymous");
        Credential as = null;
        mockMvc.perform(get(as, p1))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void updateAsRoot() throws Exception {
        log.info("Test update platform as root");
        Credential as = asRoot;
        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");
        mockMvc.perform(update(as, p1, mp))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void updateAsAdmin() throws Exception {
        log.info("Test update platform as admin");
        Credential as = asAdmin1;
        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");
        mockMvc.perform(update(as, p1, mp))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsClient() throws Exception {
        log.info("Test update platform as client");
        Credential as = asClient1;
        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");
        mockMvc.perform(update(as, p1, mp))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsUser() throws Exception {
        log.info("Test update platform as user");
        Credential as = asUser1;
        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");
        mockMvc.perform(update(as, p1, mp))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void updateAsAnonymous() throws Exception {
        log.info("Test update platform as anonymous");
        Credential as = null;
        MPlatform mp = new MPlatform();
        mp.setName("New name");
        mp.setDescription("New description");
        mockMvc.perform(update(as, p1, mp))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    @Test
    public void deleteAsRoot() throws Exception {
        log.info("Test delete platform as super");
        Credential as = asRoot;
        mockMvc.perform(delete(as, p2))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void deleteAsAdmin() throws Exception {
        log.info("Test delete platform as admin");
        Credential as = asAdmin2;
        mockMvc.perform(delete(as, p2))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsClient() throws Exception {
        log.info("Test delete platform as client");
        Credential as = asClient2;
        mockMvc.perform(delete(as, p2))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsUser() throws Exception {
        log.info("Test delete platform as user");
        Credential as = asUser1;
        mockMvc.perform(delete(as, p2))
                .andExpect(MockMvcResultMatchers.status().isForbidden());
    }

    @Test
    public void deleteAsAnonymous() throws Exception {
        log.info("Test delete platform as anonymous");
        Credential as = null;
        mockMvc.perform(delete(as, p2))
                .andExpect(MockMvcResultMatchers.status().isUnauthorized());
    }

    private MockHttpServletRequestBuilder search(Credential who) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/platforms")
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder create(MCreatePlatform mcp, Credential who) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post("/platforms")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(mcp));
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder get(Credential who, Platform toGet) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get("/platforms/{platformId}", toGet.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder update(Credential who, Platform toEdit, MPlatform edition) throws Exception {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put("/platforms/{platformId}", toEdit.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(edition));
        credentialMock.authenticate(request, who);
        return request;
    }

    private MockHttpServletRequestBuilder delete(Credential who, Platform toDelete) {
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete("/platforms/{platformId}", toDelete.getId())
                .accept(MediaType.APPLICATION_JSON);
        credentialMock.authenticate(request, who);
        return request;
    }

}
