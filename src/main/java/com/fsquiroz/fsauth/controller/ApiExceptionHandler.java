package com.fsquiroz.fsauth.controller;

import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MException;
import com.fsquiroz.fsauth.exception.AppException;
import com.fsquiroz.fsauth.exception.BadRequestException;
import com.fsquiroz.fsauth.exception.ErrorCode;
import com.fsquiroz.fsauth.security.SecurityCheck;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.connector.ClientAbortException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.data.mapping.PropertyReferenceException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import jakarta.servlet.http.HttpServletRequest;
import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
@RestControllerAdvice(annotations = RestController.class)
public class ApiExceptionHandler {

    private final SecurityCheck securityCheck;

    @ExceptionHandler(AppException.class)
    public ResponseEntity<MException> appException(HttpServletRequest req, AppException ae) {
        logUrl(req);
        log.debug(ae.getMessage(), ae);
        return parseException(req, ae);
    }

    @ExceptionHandler(PropertyReferenceException.class)
    public ResponseEntity<MException> propertyReferenceException(HttpServletRequest req, PropertyReferenceException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byInvalidSortParam(e.getType().getType(), e.getPropertyName()));
    }

    @ExceptionHandler(HttpMessageConversionException.class)
    public ResponseEntity<MException> httpMessageConversionException(HttpServletRequest req, HttpMessageConversionException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMalformedBody());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<MException> typeMismatchException(HttpServletRequest req, MethodArgumentTypeMismatchException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMalformedParam(e));
    }

    @ExceptionHandler(InvalidDataAccessApiUsageException.class)
    public ResponseEntity<MException> dataAccessApiUsageException(HttpServletRequest req, InvalidDataAccessApiUsageException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMalformedSortingParam(e.getMessage()));
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<MException> missingParameter(HttpServletRequest req, MissingServletRequestParameterException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
        return parseException(req, BadRequestException.byMissingParam(e.getParameterName()));
    }

    @ExceptionHandler(ClientAbortException.class)
    public void clientAbortException(HttpServletRequest req, ClientAbortException e) {
        logUrl(req);
        log.debug(e.getMessage(), e);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity<MException> accessDeniedException(HttpServletRequest req, AccessDeniedException e) {
        HttpStatus status = HttpStatus.FORBIDDEN;
        ErrorCode errorCode;
        Map<String, Object> meta = null;
        try {
            User u = securityCheck.getAuthenticated();
            meta = new LinkedHashMap<>();
            Map<String, Object> accessAs = new LinkedHashMap<>();
            accessAs.put("userId", u.getId());
            accessAs.put("userPlatformIdentifier", u.getPlatformIdentifier());
            accessAs.put("userRole", u.getRole());
            meta.put("accessingAs", accessAs);
            errorCode = ErrorCode.FORBIDDEN;
        } catch (AppException ae) {
            status = HttpStatus.UNAUTHORIZED;
            errorCode = ErrorCode.U_ACCESS_DENIED;
        }
        logUrl(req);
        return build(req, status, errorCode, meta, e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<MException> genericException(HttpServletRequest req, Exception e) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.error("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.error("[{}] {}", req.getMethod(), req.getRequestURL());
        }
        log.error(e.getMessage(), e);
        return parseException(req, e);
    }


    private void logUrl(HttpServletRequest req) {
        if (req.getQueryString() != null && !req.getQueryString().isEmpty()) {
            log.debug("[{}] {}?{}", req.getMethod(), req.getRequestURL(), req.getQueryString());
        } else {
            log.debug("[{}] {}", req.getMethod(), req.getRequestURL());
        }
    }

    private ResponseEntity<MException> parseException(HttpServletRequest req, Exception e) {
        ResponseStatus rs = AnnotatedElementUtils.findMergedAnnotation(e.getClass(), ResponseStatus.class);
        HttpStatus status;
        Map<String, Object> meta = null;
        ErrorCode errorCode = ErrorCode.INTERNAL_SERVER;
        if (rs != null) {
            status = rs.value();
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        if (e instanceof AppException ae) {
            meta = ae.getMeta();
            errorCode = ae.getCode();
        }
        return build(req, status, errorCode, meta, status == HttpStatus.INTERNAL_SERVER_ERROR ? "There has been an unexpected error" : e.getMessage());
    }


    private ResponseEntity<MException> build(HttpServletRequest req, HttpStatus status, ErrorCode errorCode, Map<String, Object> meta, String message) {
        MException me = new MException(
                Instant.now(),
                status.value(),
                status.getReasonPhrase(),
                message,
                req.getRequestURI(),
                meta,
                errorCode
        );
        return new ResponseEntity<>(
                me,
                status
        );
    }

}


