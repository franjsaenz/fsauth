package com.fsquiroz.fsauth.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MStatus {

    private String name;
    private String version;
    private Instant build;
    private Instant startUp;
    private String message;
}
