package com.fsquiroz.fsauth.service.declaration;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCreatePlatform;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;

public interface IPlatformService {

    @PreAuthorize("hasRole('SUPER')")
    Page<Platform> search(String term, boolean withDeleted, Pageable pageable);

    @PostAuthorize("@securityCheck.canGetPlatform(returnObject)")
    Platform get(String id);

    @PreAuthorize("hasRole('SUPER')")
    Platform create(MCreatePlatform model, User creator);

    @PreAuthorize("hasRole('SUPER')")
    Platform update(Platform original, MPlatform model, User updater);

    @PreAuthorize("hasRole('SUPER')")
    Platform delete(Platform original, User deleter);

}
