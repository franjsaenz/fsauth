package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.repository.CredentialRepository;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.security.SecurityCheck;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.*;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.mockito.ArgumentMatchers.any;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class CredentialServiceTest {

    @MockBean
    private CredentialRepository credentialRepository;

    @MockBean
    private SearchRepository searchRepository;

    @MockBean
    private SecurityCheck securityCheck;

    private CredentialService service;

    @BeforeEach
    public void setup() {
        service = new CredentialService(credentialRepository, searchRepository, securityCheck);
    }

    @Test
    public void search() {
        log.info("Test search");

        Credential c = new Credential();
        c.setId("1");
        List<Credential> list = Collections.singletonList(c);
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<Credential> page = new PageImpl<>(list, pageRequest, 1);

        Mockito.when(searchRepository.searchCredentials(any(), any(), any(), any()))
                .thenReturn(page);

        Page<Credential> resp = service.search(null, null, null, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .containsExactly(c);
    }

    @Test
    public void get() {
        log.info("Test get credential");

        Credential c = new Credential();
        c.setId("1");

        Mockito.when(credentialRepository.findById(any()))
                .thenReturn(Optional.of(c));

        Credential resp = service.get("1");

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("1");
    }

    @Test
    public void getNotFound() {
        log.info("Test get credential not found");

        Mockito.when(credentialRepository.findById(any()))
                .thenReturn(Optional.empty());

        Exception resp = null;

        try {
            service.get("1");
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isInstanceOf(NotFoundException.class);
    }

    @Test
    public void create() {
        log.info("Test create credential");

        Instant expiration = Instant.now().plus(30, ChronoUnit.MINUTES);
        Instant notBefore = Instant.now();

        User u = new User();
        u.setId("1");
        u.setCreated(Instant.now());
        u.setRole(Role.SUPER);

        Mockito.when(credentialRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Credential resp = service.create(u, null, expiration, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getUser())
                .isEqualTo(u);
        assertThat(resp.getIssuer())
                .isNull();
        assertThat(resp.getIssuedAt())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getExpiration())
                .isCloseTo(expiration, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, within(1, ChronoUnit.MILLIS));
    }

    @Test
    public void createWithoutExpiration() {
        log.info("Test create credential without expiration");

        Instant expiration = null;
        Instant notBefore = Instant.now();

        User u = new User();
        u.setId("1");
        u.setCreated(Instant.now());
        u.setRole(Role.SUPER);

        Mockito.when(credentialRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Credential resp = service.create(u, null, expiration, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getUser())
                .isEqualTo(u);
        assertThat(resp.getIssuer())
                .isNull();
        assertThat(resp.getIssuedAt())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getExpiration())
                .isCloseTo(notBefore.plus(30, ChronoUnit.MINUTES), within(1, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isCloseTo(notBefore, within(1, ChronoUnit.MILLIS));
    }

    @Test
    public void createWithoutExpirationNorNotBefore() {
        log.info("Test create credential without expiration nor notBefore");

        Instant expiration = null;
        Instant notBefore = null;

        User u = new User();
        u.setId("1");
        u.setCreated(Instant.now());
        u.setRole(Role.SUPER);

        Mockito.when(credentialRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Credential resp = service.create(u, null, expiration, notBefore);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getUser())
                .isEqualTo(u);
        assertThat(resp.getIssuer())
                .isNull();
        assertThat(resp.getIssuedAt())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getExpiration())
                .isCloseTo(Instant.now().plus(30, ChronoUnit.MINUTES), within(1, ChronoUnit.MILLIS));
        assertThat(resp.getNotBefore())
                .isNull();
    }

    @Test
    public void delete() {
        log.info("Test delete credential");

        Instant created = Instant.now().minus(3, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(1, ChronoUnit.DAYS);
        Credential c = new Credential();
        c.setId("1");
        c.setCreated(created);
        c.setUpdated(updated);

        Mockito.when(credentialRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Credential resp = service.delete(c, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
    }

    @Test
    public void logout() {
        log.info("Test logout");

        User u = new User();
        u.setId("2");

        Instant created = Instant.now().minus(3, ChronoUnit.DAYS);
        Instant updated = Instant.now().minus(1, ChronoUnit.DAYS);
        Credential c = new Credential();
        c.setId("1");
        c.setCreated(created);
        c.setUpdated(updated);
        c.setUser(u);

        service.logout(c);

        assertThat(c)
                .isNotNull();
        assertThat(c.getCreated())
                .isCloseTo(created, within(1, ChronoUnit.MILLIS));
        assertThat(c.getUpdated())
                .isCloseTo(updated, within(1, ChronoUnit.MILLIS));
        assertThat(c.getDeleted())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(c.getDeleter())
                .isEqualTo(u);
    }
}
