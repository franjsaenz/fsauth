# fsAuth

## Introduction

Microservice intended to manage users and authentication for platforms.

Implemented as a REST service, exposes functionality through http endpoints based on JSON format interchange.

## Database

This project use MongoDB for runtime and test environments.

## Getting started

### Dependencies

#### Runtime

It is required for the normal execution of this project the placement of the application properties file.
On docker context the file is automatically created when running `bash dependencies`.
Outside Docker context, the file is search under `${user.dir}/.fsauth/application.properties`, for example: `/home/user/.fsauth/application.properties`. The fields required in such file are:

    - spring.data.mongodb.database [string]: Database name
    - spring.data.mongodb.uri [string]: Connection URI to MongoDB
    - security.jwt.secret [string]: Secret key to encrypt the authentication token. Recomended a randomly generate string with uppercases, lowercases and numbers of no less than 86 characters

Example of `application.properties` file:

```
spring.data.mongodb.database=fs-auth
spring.data.mongodb.uri=mongodb://username:secretPassword@localhost:27017/
security.jwt.secret=fgd8fgE2WTW4sdv8sdfwerwe6sdSGFwen345jETYJsDb34dsf5u3dfgy6yjt4sdff
```

### Running with Docker

This application has been prepared to be run using Docker.

#### Dependencies

This project can be run on locally with all dependencies covered in a `docker-compose`. To start dependencies execute `bash dependencies` which run the docker-compose exposing:
- MongoDB on port 27017
- Mongo Express on port 8081

#### Dependencies and app

This project can be run locally as a Docker Image.
If is the first time running this project in a machine, the image needs to be built with `bash build`
Once the image is built, executing `bash run` will start up project and dependencies exposing:
- fsAuth on port 8080
- MongoDB on port 27017
- Mongo Express on port 8081

### Running standalone

First, we need the application deployed in some environment, we will assume is under `http://localhost:8080`.
We can use the embedded documentation in `http://localhost:8080/swagger-ui.html` or some other http tool such as cUrl or Postman.

To run the project from source code, maven is mandatory, and the command to execute is:

```
mvn clean spring-boot:run
```

### First login

- **[POST] */login***

On first start up, super user is created automatically with the following credentials:

    - platformIdetifier: super@fsauth.com
    - password: 123456

It is highly advice to change this password as son as possible.

Request body fields:

    - platformIdetifier [type: string, mandatory]: User's unique platform identifier. Can be an email, phone nomber, or any other string.
    - password [type: string, optional]: User's password. Mandatory for password method login.
    - token [type: string, optional]: User's login token. Mandatory for passwordless method login.

Request params:

    - method [type: string, optional]: Login method. Values can take: [PASSWORD, PASSWORDLESS]. If leaved in blank, asume PASSWORD.
    - expiration [type: DateTime, optional]: Credential issued can be used up to the given date. If leaved in blank, never expires, except super user, a expiration will be set for the next 30 minutes.
    - notBefore [type: DateTime, optional]: Credential issued can be used starting the given date. If leaved blank, can be used inmidiatly.

Request body example:

```
{
  "platformIdetifier": "super@fsauth.com",
  "password": "123456"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/login?method=PASSWORDLESS \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "platformIdetifier": "super@fsauth.com", "password": "123456"}'
```

Response example:

```
{
  "id": "5d39e67c741f8a53f78ebf6a",
  "created": "2019-07-25T17:27:24.644Z",
  "user": {
    "id": "5d2637e904d5eb6e4cbcde23",
    "created": "2019-07-10T19:09:29.048Z",
    "role": "SUPER",
    "platformIdentifier": "super@fsauth.com"
  },
  "issuedAt": "2019-07-25T17:27:24.644Z",
  "expiration": "2019-07-25T17:57:24.644Z",
  "token": "eyJ..."
}
```

From this response is that the authentication token can be obtained. This is set in the header in the following way: `Authorization:Bearer eyJ...`

### Get authenticated

With the authentication token setup in the request, the logged in user can be easily obtain:

Request cUrl example command:

```
curl -X GET \
  http://localhost:8080/login \
  -H 'accept: */*' \
  -H "Authorization: Bearer eyJ..."
```

Response example:

```
{
  "id": "5d39e67c741f8a53f78ebf6a",
  "created": "2019-07-25T17:27:24.644Z",
  "user": {
    "id": "5d2637e904d5eb6e4cbcde23",
    "created": "2019-07-10T19:09:29.048Z",
    "role": "SUPER",
    "platformIdentifier": "super@fsauth.com"
  },
  "issuedAt": "2019-07-25T17:27:24.644Z",
  "expiration": "2019-07-25T17:57:24.644Z"
}
```

### Change first password

- **[POST] */users/{userId}/password***

After the initial login, it is highly advisable to change the password, here is how.

Request url fields:

    - userId [type: string, mandatory]: User id get in the login step.

Request header fields:

    - Authorization [type: string, mandatory]: Bearer token obtained in the login preced by Bearer and an space
    
Request body fields:

    - oldPassword [type: string, optional]: Old user's password. Optional for accounts without previos password.
    - newPassword [type: string, mandatory]: Nuw user's password.

Request body example:

```
{
  "oldPassword": "123456",
  "newPassword": "some-new-and-secure-p4ssw0rd"
}
```

Request cUrl example command:

```
curl -X POST \
  http://localhost:8080/users/5d2637e904d5eb6e4cbcde23/password \
  -H 'Authorization: Bearer eyJ...' \
  -H 'Content-Type: application/json' \
  -H 'accept: */*' \
  -d '{ "oldPassword": "123456", "newPassword": "some-new-and-secure-p4ssw0rd"}'
```

Response example:

```
{
  "timestamp": "2019-04-20T19:48:20.339+0000",
  "message": "Password updated"
}
```


### The rest

All endpoints are fully documented in Swagger, exposing all the flexibility of this tool.
