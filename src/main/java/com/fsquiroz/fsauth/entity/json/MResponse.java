package com.fsquiroz.fsauth.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MResponse {

    private Instant timestamp;
    private String message;

    public static MResponse of(String message) {
        return new MResponse(Instant.now(), message);
    }
}
