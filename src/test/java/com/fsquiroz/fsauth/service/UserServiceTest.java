package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.*;
import com.fsquiroz.fsauth.entity.json.MLogin;
import com.fsquiroz.fsauth.entity.json.MResetPassword;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.exception.BadRequestException;
import com.fsquiroz.fsauth.exception.UnauthorizedException;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.repository.TokenRepository;
import com.fsquiroz.fsauth.repository.UserRepository;
import com.fsquiroz.fsauth.service.declaration.ICredentialService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.internal.stubbing.answers.ReturnsArgumentAt;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.within;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;

@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class UserServiceTest {

    @MockBean
    private PasswordEncoder encoder;

    @MockBean
    private ICredentialService credentialService;

    @MockBean
    private UserRepository userRepository;

    @MockBean
    private TokenRepository tokenRepository;

    @MockBean
    private SearchRepository searchRepository;

    private UserService service;

    @BeforeEach
    public void setup() {
        service = new UserService(encoder, credentialService, userRepository, tokenRepository, searchRepository);
    }

    @Test
    public void loginWithPassword() {
        log.info("Test login as super with password");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("super");
        u.setRole(Role.SUPER);
        Credential c = new Credential();
        c.setId("2");
        c.setUser(u);

        MLogin login = new MLogin();
        login.setPlatformIdentifier("super");
        login.setPassword("password");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(encoder.matches(any(), any()))
                .thenReturn(true);
        Mockito.when(credentialService.create(any(), any(), any(), any()))
                .thenReturn(c);

        Credential resp = service.login(login, LoginMethod.PASSWORD, null, null, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isEqualTo(c);
    }

    @Test
    public void loginPasswordlessFirstStep() {
        log.info("Login as user passwordless first step");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.save(any()))
                .thenReturn(t);

        Credential resp = service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("not-valid");
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getIssuer())
                .isEqualTo(issuer);
        assertThat(resp.getIssuedAt())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getExpiration())
                .isCloseTo(Instant.now().plus(30, ChronoUnit.MINUTES), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUser())
                .isEqualTo(u);
        assertThat(resp.getToken())
                .isEqualTo("token");
    }

    @Test
    public void loginPasswordlessSecondStep() {
        log.info("Login as user passwordless second step");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));
        Credential c = new Credential();
        c.setId("2");
        c.setUser(u);

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));
        Mockito.when(credentialService.create(any(), any(), any(), any()))
                .thenReturn(c);

        Credential resp = service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isEqualTo(c);
    }

    @Test
    public void loginInvalidIssuer() {
        log.info("Test login passwordless with invalid issuer");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_USER);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessWithoutIssuer() {
        log.info("Test login passwordless withot issuer");

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, null, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginWithoutMethod() {
        log.info("Test login without method");

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Exception resp = null;

        try {
            service.login(login, null, null, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void loginUserNotFound() {
        log.info("Test login with user not found");

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setPassword("password");

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORD, null, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginDeletedUser() {
        log.info("Test login deleted user");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("user");
        u.setDeleted(Instant.now());
        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setPassword("password");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORD, null, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginWrongPassword() {
        log.info("Test login wrong password");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("user");
        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setPassword("password");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(encoder.matches(any(), any()))
                .thenReturn(false);

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORD, null, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessAsSuper() {
        log.info("Test login passworless as super");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("user");
        u.setRole(Role.SUPER);
        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, u, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessTokenNotFound() {
        log.info("Login as user passwordless with incorrect token");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessTokenForOtherUser() {
        log.info("Login as user passwordless with token for other user");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        User o = new User();
        o.setId("3");
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(o);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessTokenAlreadyUsed() {
        log.info("Login as user passwordless with token already used");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setDeleted(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessMismatchTokenType() {
        log.info("Login as user passwordless with mismatch token type");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.RESET_PASSWORD);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void loginPasswordlessExpiredToken() {
        log.info("Login as user passwordless with expired token");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(u);
        t.setExpiration(Instant.now().minus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");
        login.setToken("token");

        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(u));
        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));

        Exception resp = null;

        try {
            service.login(login, LoginMethod.PASSWORDLESS, issuer, null, null);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void search() {
        log.info("Test search");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("some-identifier");
        List<User> list = Collections.singletonList(u);
        Pageable pageRequest = PageRequest.of(0, 10);
        Page<User> page = new PageImpl<>(list, pageRequest, 1);

        Mockito.when(searchRepository.searchUsers(any(), any(), any(), anyBoolean(), any()))
                .thenReturn(page);

        Page<User> resp = service.search(null, null, null, false, pageRequest);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .hasSize(1);
        assertThat(resp)
                .containsExactly(u);
    }

    @Test
    public void get() {
        log.info("Test get user");

        User u = new User();
        u.setId("1");
        u.setPlatformIdentifier("some-identifier");

        Mockito.when(userRepository.findById(any()))
                .thenReturn(Optional.of(u));

        User resp = service.get("1");

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("1");
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("some-identifier");
    }

    @Test
    public void createWithPassword() {
        log.info("Test create user with password");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User creator = new User();
        creator.setId("1");
        creator.setRole(Role.SUPER);
        creator.setPlatformIdentifier("super");

        MUser mu = new MUser();
        mu.setPlatformIdentifier("other");
        mu.setRole(Role.PLATFORM_ADMIN);
        mu.setPassword("some");

        Mockito.when(encoder.encode(any()))
                .thenReturn("encoded");
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.create(mu, creator, p);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(Role.PLATFORM_ADMIN);
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("other");
        assertThat(resp.getPassword())
                .isEqualTo("encoded");
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getPasswordType())
                .isEqualTo(User.PasswordType.V_1);
        assertThat(resp.getCreator())
                .isEqualTo(creator);
    }

    @Test
    public void createWithoutPassword() {
        log.info("Test create passwordless user");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User creator = new User();
        creator.setId("1");
        creator.setRole(Role.SUPER);
        creator.setPlatformIdentifier("super");

        MUser mu = new MUser();
        mu.setPlatformIdentifier("other");
        mu.setRole(Role.PLATFORM_ADMIN);

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.create(mu, creator, p);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(Role.PLATFORM_ADMIN);
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("other");
        assertThat(resp.getPassword())
                .isNull();
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getPasswordType())
                .isEqualTo(User.PasswordType.PASSWORDLESS);
        assertThat(resp.getCreator())
                .isEqualTo(creator);
    }

    @Test
    public void createClient() {
        log.info("Test create client");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User creator = new User();
        creator.setId("1");
        creator.setRole(Role.SUPER);
        creator.setPlatformIdentifier("super");

        MUser mu = new MUser();
        mu.setPlatformIdentifier("other");
        mu.setRole(Role.PLATFORM_CLIENT);

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.create(mu, creator, p);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(Role.PLATFORM_CLIENT);
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("other");
        assertThat(resp.getPassword())
                .isNull();
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getPasswordType())
                .isEqualTo(User.PasswordType.NOT_SET);
        assertThat(resp.getCreator())
                .isEqualTo(creator);
    }

    @Test
    public void createSuperNotBeingSuper() {
        log.info("Test create user with role super not being super");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User creator = new User();
        creator.setId("1");
        creator.setRole(Role.PLATFORM_USER);
        creator.setPlatformIdentifier("super");

        MUser mu = new MUser();
        mu.setPlatformIdentifier("other");
        mu.setRole(Role.SUPER);
        mu.setPassword("some");

        Mockito.when(encoder.encode(any()))
                .thenReturn("encoded");
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        Exception resp = null;

        try {
            service.create(mu, creator, p);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void createExistingInPlatform() {
        log.info("Test create user already existing in platform");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User creator = new User();
        creator.setId("1");
        creator.setRole(Role.SUPER);
        creator.setPlatformIdentifier("super");

        MUser mu = new MUser();
        mu.setPlatformIdentifier("other");
        mu.setRole(Role.PLATFORM_ADMIN);
        mu.setPassword("some");

        Mockito.when(encoder.encode(any()))
                .thenReturn("encoded");
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));
        Mockito.when(userRepository.findByPlatformAndPlatformIdentifier(any(), any()))
                .thenReturn(Optional.of(new User()));

        Exception resp = null;

        try {
            service.create(mu, creator, p);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(BadRequestException.class);
    }

    @Test
    public void update() {
        log.info("Test update user");

        Instant created = Instant.now();

        Map<String, Object> attributes = new LinkedHashMap<>();
        attributes.put("some", "other");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Platform");

        User updater = new User();
        updater.setId("1");
        updater.setRole(Role.SUPER);
        updater.setPlatformIdentifier("super");

        User user = new User();
        user.setId("2");
        user.setCreated(created);
        user.setCreator(updater);
        user.setPlatform(p);
        user.setRole(Role.PLATFORM_ADMIN);
        user.setPlatformIdentifier("other");

        MUser mu = new MUser();
        mu.setAttributes(attributes);

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.update(user, mu, updater);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getDeleted())
                .isNull();
        assertThat(resp.getRole())
                .isEqualTo(Role.PLATFORM_ADMIN);
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("other");
        assertThat(resp.getPassword())
                .isNull();
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getCreator())
                .isEqualTo(updater);
        assertThat(resp.getUpdater())
                .isEqualTo(updater);
    }

    @Test
    public void delete() {
        log.info("Test delete user");

        Instant created = Instant.now().minus(3, ChronoUnit.DAYS);

        User deleter = new User();
        deleter.setId("1");
        deleter.setRole(Role.SUPER);
        deleter.setPlatformIdentifier("super");

        User user = new User();
        user.setId("2");
        user.setCreated(created);
        user.setCreator(deleter);
        user.setRole(Role.PLATFORM_ADMIN);
        user.setPlatformIdentifier("other");

        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.delete(user, deleter);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getCreated())
                .isCloseTo(created, within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUpdated())
                .isNull();
        assertThat(resp.getDeleted())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getRole())
                .isEqualTo(Role.PLATFORM_ADMIN);
        assertThat(resp.getPlatformIdentifier())
                .isEqualTo("other");
        assertThat(resp.getPassword())
                .isNull();
    }

    @Test
    public void changePassword() {
        log.info("Test change password");

        User u = new User();
        u.setId("1");
        u.setPassword("password");

        MResetPassword mrp = new MResetPassword();
        mrp.setNewPassword("new");
        mrp.setOldPassword("password");

        Mockito.when(encoder.matches(any(), any()))
                .thenReturn(true);
        Mockito.when(encoder.encode(any()))
                .thenReturn("encoded");
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.changePassword(u, mrp);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isEqualTo(u);
        assertThat(u.getPassword())
                .isEqualTo("encoded");
    }

    @Test
    public void changeUnmatchedPassword() {
        log.info("Test change unmatched password");

        User u = new User();
        u.setId("1");
        u.setPassword("password");

        MResetPassword mrp = new MResetPassword();
        mrp.setNewPassword("new");
        mrp.setOldPassword("password");

        Mockito.when(encoder.matches(any(), any()))
                .thenReturn(false);

        Exception resp = null;

        try {
            service.changePassword(u, mrp);
        } catch (Exception e) {
            resp = e;
        }

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isExactlyInstanceOf(UnauthorizedException.class);
    }

    @Test
    public void resetPasswordFirstStep() {
        log.info("Reset password first step");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.LOGIN);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));

        MLogin login = new MLogin();
        login.setPlatformIdentifier("user");

        Mockito.when(tokenRepository.save(any()))
                .thenReturn(t);

        Credential resp = service.resetPassword(u, issuer);

        assertThat(resp)
                .isNotNull();
        assertThat(resp.getId())
                .isEqualTo("not-valid");
        assertThat(resp.getPlatform())
                .isEqualTo(p);
        assertThat(resp.getIssuer())
                .isEqualTo(issuer);
        assertThat(resp.getIssuedAt())
                .isCloseTo(Instant.now(), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getExpiration())
                .isCloseTo(Instant.now().plus(30, ChronoUnit.MINUTES), within(500, ChronoUnit.MILLIS));
        assertThat(resp.getUser())
                .isEqualTo(u);
        assertThat(resp.getToken())
                .isEqualTo("token");
    }

    @Test
    public void resetPasswordSecondStep() {
        log.info("Reset password second step");

        Platform p = new Platform();
        p.setId("1");
        p.setName("Name");
        User issuer = new User();
        issuer.setId("1");
        issuer.setPlatform(p);
        issuer.setPlatformIdentifier("admin");
        issuer.setRole(Role.PLATFORM_ADMIN);
        User u = new User();
        u.setId("2");
        u.setPlatform(p);
        u.setPlatformIdentifier("user");
        u.setRole(Role.PLATFORM_USER);
        Token t = new Token();
        t.setId("token");
        t.setCreated(Instant.now());
        t.setPlatform(p);
        t.setCreator(issuer);
        t.setType(Token.Type.RESET_PASSWORD);
        t.setUser(u);
        t.setExpiration(Instant.now().plus(30, ChronoUnit.MINUTES));
        Credential c = new Credential();
        c.setId("2");
        c.setUser(u);

        MResetPassword mrp = new MResetPassword();
        mrp.setNewPassword("new");
        mrp.setToken("token");

        Mockito.when(tokenRepository.findById(any()))
                .thenReturn(Optional.of(t));
        Mockito.when(encoder.encode(any()))
                .thenReturn("encoded");
        Mockito.when(userRepository.save(any()))
                .then(new ReturnsArgumentAt(0));

        User resp = service.resetPassword(u, mrp, issuer);

        assertThat(resp)
                .isNotNull();
        assertThat(resp)
                .isEqualTo(u);
        assertThat(u.getPassword())
                .isEqualTo("encoded");
    }

}
