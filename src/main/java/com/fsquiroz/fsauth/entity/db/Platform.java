package com.fsquiroz.fsauth.entity.db;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter
@ToString(callSuper = true)
public class Platform extends Entity {

    private String name;

    private String description;

}
