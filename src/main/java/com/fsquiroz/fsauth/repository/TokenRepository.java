package com.fsquiroz.fsauth.repository;

import com.fsquiroz.fsauth.entity.db.Token;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TokenRepository extends MongoRepository<Token, String> {
}
