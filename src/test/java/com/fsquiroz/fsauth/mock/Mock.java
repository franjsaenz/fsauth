package com.fsquiroz.fsauth.mock;

import com.fsquiroz.fsauth.entity.db.Entity;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.UUID;

public abstract class Mock<T extends Entity> {

    protected MongoRepository<T, String> repository;

    protected Mock(MongoRepository<T, String> repository) {
        this.repository = repository;
    }

    public void cleanup() {
        repository.deleteAll();
    }

    protected T save(T t) {
        t = repository.save(t);
        return t;
    }

    protected String mockString() {
        return UUID.randomUUID().toString();
    }

}
