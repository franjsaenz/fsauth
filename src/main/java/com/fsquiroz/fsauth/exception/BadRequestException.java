package com.fsquiroz.fsauth.exception;

import com.fsquiroz.fsauth.entity.db.Entity;
import com.fsquiroz.fsauth.entity.db.LoginMethod;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import org.springframework.http.HttpStatus;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import java.time.Instant;
import java.util.LinkedHashMap;
import java.util.Map;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends AppException {

    private BadRequestException(ErrorCode errorCode, Map<String, Object> meta, String message) {
        super(errorCode, meta, message);
    }

    public static BadRequestException byMissingParam(String param) {
        Assert.hasText(param, "'param' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_MISSING_PARAM, meta, "Missing param");
    }

    public static BadRequestException byMissingParam(Platform platform, String userId, String platformIdentifier) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("platform", platform != null ? ofEntity(platform) : null);
        meta.put("userId", userId);
        meta.put("platformIdentifier", platform);
        return new BadRequestException(ErrorCode.BR_MISSING_PARAM, meta, "Missing param");
    }

    public static BadRequestException byModifyingDeleted(Entity entity) {
        Map<String, Object> meta = ofEntity(entity);
        meta.put("deleted", entity.getDeleted());
        return new BadRequestException(ErrorCode.BR_EDIT_DELETED, meta, "Can not modify a deleted entity");
    }

    public static BadRequestException byExistingPlatformIdentifier(String platformIdentifier) {
        Assert.hasText(platformIdentifier, "'platformIdentifier' must not be empty");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("platformIdentifier", platformIdentifier);
        return new BadRequestException(ErrorCode.BR_EXISTING_EMAIL, meta, "Already exist a user with this platform identifier");
    }

    public static BadRequestException byInvalidLoginMethod(LoginMethod method) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("method", method);
        return new BadRequestException(ErrorCode.BR_INVALID_LOGIN_METHOD, meta, "Invalid login method");
    }

    public static BadRequestException byNonSuperCreatingSuper(User who) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("user", who != null ? ofUser(who) : null);
        return new BadRequestException(ErrorCode.BR_NON_SUPER_CREATING_SUPER, meta, "Only 'super' user can create users with 'super' role");
    }

    public static BadRequestException byInvalidSortParam(Class clazz, String param) {
        Assert.notNull(clazz, "'clazz' can not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", clazz.getSimpleName());
        meta.put("param", param);
        return new BadRequestException(ErrorCode.BR_INVALID_SORT_PARAM, meta, "Invalid sort param");
    }

    public static BadRequestException byMalformedBody() {
        return new BadRequestException(ErrorCode.BR_MALFORMED_BODY, null, "Unable to parse body content");
    }

    public static BadRequestException byMalformedParam(MethodArgumentTypeMismatchException e) {
        Map<String, Object> meta = null;
        if (e != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", e.getName() != null ? e.getName() : e.getPropertyName());
            meta.put("value", e.getValue());
            meta.put("requiredType", e.getRequiredType() != null ? e.getRequiredType().getSimpleName() : "UNKNOWN");
            meta.put("message", e.getMessage());
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    public static BadRequestException byMalformedSortingParam(String message) {
        Map<String, Object> meta = null;
        if (message != null) {
            meta = new LinkedHashMap<>();
            meta.put("param", "sort");
            meta.put("message", message);
        }
        return new BadRequestException(ErrorCode.BR_MALFORMED_PARAM, meta, "Unable to parse param");
    }

    public static BadRequestException byExpirationBefore(Instant expiration, Instant notBefore) {
        Assert.notNull(expiration, "'expiration' must not be null");
        Assert.notNull(notBefore, "'notBefore' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("expiration", expiration);
        meta.put("notBefore", notBefore);
        return new BadRequestException(ErrorCode.BR_EXPIRATION_BEFORE_NOT_BEFORE, meta, "'expiration' can not be before from 'notBefore'");
    }

    private static Map<String, Object> ofEntity(Entity entity) {
        Assert.notNull(entity, "'entity' must not be null");
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("entity", entity.getClass().getSimpleName());
        meta.put("id", entity.getId());
        return meta;
    }

    private static Map<String, Object> ofUser(User user) {
        Map<String, Object> meta = new LinkedHashMap<>();
        meta.put("id", user.getId());
        if (user.getPlatform() != null) {
            meta.put("platformId", user.getPlatform().getId());
        }
        meta.put("platformIdentifier", user.getPlatformIdentifier());
        meta.put("role", user.getRole());
        return meta;
    }

}
