package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCredential;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.service.declaration.IBuildService;
import org.springframework.stereotype.Service;

@Service
public class BuildService implements IBuildService {

    @Override
    public MUser user(User user) {
        MUser mu = null;
        if (user != null) {
            mu = new MUser();
            mu.setPlatformIdentifier(user.getPlatformIdentifier());
            mu.setPlatform(platform(user.getPlatform()));
            mu.setId(user.getId());
            mu.setCreated(user.getCreated());
            mu.setUpdated(user.getUpdated());
            mu.setDeleted(user.getDeleted());
            mu.setRole(user.getRole());
            mu.setAttributes(user.getAttributes());
        }
        return mu;
    }

    @Override
    public MCredential credential(Credential c) {
        MCredential mc = null;
        if (c != null) {
            mc = new MCredential();
            mc.setId(c.getId());
            mc.setCreated(c.getCreated());
            mc.setUpdated(c.getUpdated());
            mc.setDeleted(c.getDeleted());
            mc.setUser(user(c.getUser()));
            mc.setIssuer(user(c.getIssuer()));
            mc.setIssuedAt(c.getIssuedAt());
            mc.setExpiration(c.getExpiration());
            mc.setNotBefore(c.getNotBefore());
            mc.setToken(c.getToken());
        }
        return mc;
    }

    @Override
    public MPlatform platform(Platform p) {
        MPlatform mp = null;
        if (p != null) {
            mp = new MPlatform();
            mp.setId(p.getId());
            mp.setCreated(p.getCreated());
            mp.setUpdated(p.getUpdated());
            mp.setDeleted(p.getDeleted());
            mp.setName(p.getName());
            mp.setDescription(p.getDescription());
        }
        return mp;
    }

}
