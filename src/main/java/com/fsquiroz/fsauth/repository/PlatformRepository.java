package com.fsquiroz.fsauth.repository;

import com.fsquiroz.fsauth.entity.db.Platform;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlatformRepository extends MongoRepository<Platform, String> {
}
