package com.fsquiroz.fsauth.controller;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.LoginMethod;
import com.fsquiroz.fsauth.entity.json.*;
import com.fsquiroz.fsauth.exception.UnauthorizedException;
import com.fsquiroz.fsauth.security.SecurityCheck;
import com.fsquiroz.fsauth.service.declaration.IBuildService;
import com.fsquiroz.fsauth.service.declaration.ICredentialService;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class PublicController {

    private final MStatus status;

    private final ICredentialService credentialService;

    private final IUserService userService;

    private final IBuildService buildService;

    private final SecurityCheck securityCheck;

    @GetMapping
    public ResponseEntity<?> index() {
        return ResponseEntity.ok(status);
    }

    @GetMapping("/login")
    public ResponseEntity<?> login() {
        Credential c = securityCheck.getAuthentication().orElseThrow(UnauthorizedException::byInsufficientPermission);
        return ResponseEntity.ok(buildService.credential(c));
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(
            @RequestBody MLogin login,
            @RequestParam(defaultValue = "PASSWORD") LoginMethod method,
            @RequestParam(required = false) Instant expiration,
            @RequestParam(required = false) Instant notBefore
    ) {
        Credential c = securityCheck.getAuthentication().orElse(null);
        Credential credential = userService.login(
                login,
                method,
                c != null ? c.getUser() : null,
                expiration,
                notBefore
        );
        return ResponseEntity.ok(buildService.credential(credential));
    }

    @DeleteMapping("/login")
    @PreAuthorize("isAuthenticated()")
    public ResponseEntity<?> logout(@AuthenticationPrincipal Credential credential) {
        credentialService.logout(credential);
        return ResponseEntity.ok(MResponse.of("Logged out"));
    }
}
