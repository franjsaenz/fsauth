package com.fsquiroz.fsauth.config;

import com.fsquiroz.fsauth.entity.json.MStatus;
import org.springframework.context.annotation.*;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.Instant;

import static org.springframework.data.web.config.EnableSpringDataWebSupport.PageSerializationMode.VIA_DTO;

@Configuration
@Profile("test")
@PropertySources({
        @PropertySource("file:${user.home}${file.separator}.fsauth${file.separator}application-test.properties"),
        @PropertySource("classpath:application-test.properties")
})
@EnableSpringDataWebSupport(pageSerializationMode = VIA_DTO)
public class TestBeans {

    @Bean
    @Primary
    public MStatus status() {
        return new MStatus("fsAuth", "TESTING", Instant.now(), Instant.now(), "Up");
    }

    @Bean
    @Primary
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {

            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword == null ? null : rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword == null ? encodedPassword == null : rawPassword.toString().equals(encodedPassword);
            }
        };
    }

}
