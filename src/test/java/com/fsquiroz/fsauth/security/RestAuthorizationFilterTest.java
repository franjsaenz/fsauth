package com.fsquiroz.fsauth.security;

import com.fsquiroz.fsauth.entity.db.Credential;
import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import jakarta.servlet.FilterChain;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@Slf4j
public class RestAuthorizationFilterTest {

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain filterChain;

    @Mock
    private SecurityCheck securityCheck;

    @InjectMocks
    private RestAuthorizationFilter filter;

    @AfterEach
    void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    @SneakyThrows
    void testAuthenticateSuper() {
        var user = new User();
        user.setRole(Role.SUPER);

        var cred = new Credential();
        cred.setUser(user);

        when(securityCheck.authenticate(any())).thenReturn(cred);
        var request = mockRequest("abc-123", null);

        filter.doFilterInternal(request, response, filterChain);

        var authenticated = SecurityContextHolder.getContext().getAuthentication();

        assertThat(authenticated)
                .isNotNull()
                .extracting(Authentication::getPrincipal)
                .isInstanceOf(Credential.class)
                .usingRecursiveAssertion()
                .isEqualTo(cred);
    }

    @Test
    @SneakyThrows
    void testAuthenticateUser() {
        var platform = new Platform();
        platform.setId("abc123");

        var user = new User();
        user.setRole(Role.PLATFORM_USER);
        user.setPlatform(platform);

        var cred = new Credential();
        cred.setUser(user);
        cred.setPlatform(platform);

        when(securityCheck.authenticate(any())).thenReturn(cred);
        var request = mockRequest("Bearer abc-123", "abc123");

        filter.doFilterInternal(request, response, filterChain);

        var authenticated = SecurityContextHolder.getContext().getAuthentication();

        assertThat(authenticated)
                .isNotNull()
                .extracting(Authentication::getPrincipal)
                .isInstanceOf(Credential.class)
                .usingRecursiveAssertion()
                .isEqualTo(cred);
    }

    @Test
    @SneakyThrows
    void testAuthenticateUserInvalidPlatform() {
        var platform = new Platform();
        platform.setId("abc123");

        var user = new User();
        user.setRole(Role.PLATFORM_USER);
        user.setPlatform(platform);

        var cred = new Credential();
        cred.setUser(user);
        cred.setPlatform(platform);

        when(securityCheck.authenticate(any())).thenReturn(cred);
        var request = mockRequest("Bearer abc-123", "def456");

        filter.doFilterInternal(request, response, filterChain);

        var authenticated = SecurityContextHolder.getContext().getAuthentication();

        assertThat(authenticated)
                .isNull();
    }

    @Test
    @SneakyThrows
    void testAuthenticateUserWithoutHeaders() {
        var request = mockRequest(null, null);

        filter.doFilterInternal(request, response, filterChain);

        var authenticated = SecurityContextHolder.getContext().getAuthentication();

        assertThat(authenticated)
                .isNull();
    }

    @Test
    @SneakyThrows
    void testAuthenticateInvalidToken() {
        when(securityCheck.authenticate(any())).thenThrow(RuntimeException.class);
        var request = mockRequest("abc-123", null);

        filter.doFilterInternal(request, response, filterChain);

        var authenticated = SecurityContextHolder.getContext().getAuthentication();

        assertThat(authenticated)
                .isNull();
    }

    private HttpServletRequest mockRequest(String token, String platformId) {
        var request = mock(HttpServletRequest.class);
        when(request.getHeader("Authorization")).thenReturn(token);
        when(request.getHeader("Platform-Id")).thenReturn(platformId);
        return request;
    }
}
