package com.fsquiroz.fsauth.entity.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MCredential {

    private String id;
    private Instant created;
    private Instant updated;
    private Instant deleted;
    private MUser user;
    private MUser issuer;
    private Instant issuedAt;
    private Instant expiration;
    private Instant notBefore;
    private String token;
}
