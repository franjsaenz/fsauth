package com.fsquiroz.fsauth.repository;

import com.fsquiroz.fsauth.entity.db.Credential;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CredentialRepository extends MongoRepository<Credential, String> {
}
