package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Entity;
import com.fsquiroz.fsauth.exception.BadRequestException;

import java.time.Instant;

public class ValidationUtils {

    public static void notNull(Object o, String name) {
        if (o == null) {
            throw BadRequestException.byMissingParam(name);
        }
    }

    public static void notEmpty(String s, String name) {
        notNull(s, name);
        if (s.isEmpty()) {
            throw BadRequestException.byMissingParam(name);
        }
    }

    public static void notDeleted(Entity e, String name) {
        notNull(e, name);
        if (e.getDeleted() != null) {
            throw BadRequestException.byModifyingDeleted(e);
        }
    }

    public static void notBeforeNotAfterExpiration(Instant expiration, Instant notBefore) {
        if (expiration != null && notBefore != null && notBefore.isAfter(expiration)) {
            throw BadRequestException.byExpirationBefore(expiration, notBefore);
        }
    }

}
