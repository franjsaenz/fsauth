package com.fsquiroz.fsauth.service;

import com.fsquiroz.fsauth.entity.db.Platform;
import com.fsquiroz.fsauth.entity.db.Role;
import com.fsquiroz.fsauth.entity.db.User;
import com.fsquiroz.fsauth.entity.json.MCreatePlatform;
import com.fsquiroz.fsauth.entity.json.MPlatform;
import com.fsquiroz.fsauth.entity.json.MUser;
import com.fsquiroz.fsauth.exception.NotFoundException;
import com.fsquiroz.fsauth.repository.PlatformRepository;
import com.fsquiroz.fsauth.repository.SearchRepository;
import com.fsquiroz.fsauth.service.declaration.IPlatformService;
import com.fsquiroz.fsauth.service.declaration.IUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.Instant;

@Service
@RequiredArgsConstructor
public class PlatformService implements IPlatformService {

    private final IUserService userService;

    private final PlatformRepository platformRepository;

    private final SearchRepository searchRepository;

    @Override
    public Page<Platform> search(String term, boolean withDeleted, Pageable pageable) {
        return searchRepository.searchPlatforms(term, withDeleted, pageable);
    }

    @Override
    public Platform get(String id) {
        return platformRepository.findById(id).orElseThrow(() -> NotFoundException.byId(Platform.class, id));
    }

    @Override
    public Platform create(MCreatePlatform model, User creator) {
        ValidationUtils.notNull(model, "platform");
        ValidationUtils.notEmpty(model.getName(), "name");
        ValidationUtils.notEmpty(model.getDescription(), "description");
        ValidationUtils.notEmpty(model.getPlatformIdentifier(), "platformIdentifier");
        ValidationUtils.notEmpty(model.getPassword(), "password");
        Platform p = new Platform();
        p.setCreated(Instant.now());
        p.setCreator(creator);
        p.setName(model.getName());
        p.setDescription(model.getDescription());
        p = platformRepository.save(p);
        try {
            MUser mu = new MUser();
            mu.setPlatformIdentifier(model.getPlatformIdentifier());
            mu.setPassword(model.getPassword());
            mu.setRole(Role.PLATFORM_ADMIN);
            userService.create(mu, creator, p);
        } catch (Exception e) {
            platformRepository.delete(p);
            throw e;
        }
        return p;
    }

    @Override
    public Platform update(Platform original, MPlatform model, User updater) {
        ValidationUtils.notDeleted(original, "entity");
        ValidationUtils.notNull(model, "platform");
        ValidationUtils.notEmpty(model.getName(), "name");
        ValidationUtils.notEmpty(model.getDescription(), "description");
        original.setName(model.getName());
        original.setDescription(model.getDescription());
        original.setUpdated(Instant.now());
        original.setUpdater(updater);
        return platformRepository.save(original);
    }

    @Override
    public Platform delete(Platform original, User deleter) {
        ValidationUtils.notDeleted(original, "entity");
        original.setDeleted(Instant.now());
        original.setDeleter(deleter);
        return platformRepository.save(original);
    }

}
