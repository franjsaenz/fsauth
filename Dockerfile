# syntax=docker/dockerfile:1

FROM amazoncorretto:21

WORKDIR /app

EXPOSE 8080

COPY .mvn/ .mvn
COPY mvnw pom.xml ./
COPY src ./src
COPY src/main/resources/docker/docker.properties /root/.fsauth/application.properties

RUN yum update -y && yum install -y tar gzip
RUN ./mvnw dependency:resolve

VOLUME /root/.m2

CMD ["./mvnw", "spring-boot:run"]
